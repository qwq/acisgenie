#!/usr/bin/env python3
"""
The purpose of this script is to replace the dynamic library paths inside
executables with hardcoded paths, to get around Apple's System Integrity
Protection (SIP) on Macs that render DYLD_LIBRARY_PATH and LD_LIBRARY_PATH
ineffective in many situations.

For example, as of 10.14 Mojave, the user can export LD_LIBRARY_PATH in shell
to make it available for executables run from the command line, but this is
ignored by /usr/bin/env and therefore has no effect on subprocesses spawned by
Python.

One way to deal with this is to create a folder in which symlinks to needed
dylib files are placed. `@rpath` is then replaced by this path. Note that
there is a limit to the length of the path string so the path to that folder
needs to be short.

Please check the `repl` dictionary in this script and modify the list to
include the specific dylib file you require.

Usage:

change_dylib.py exefile dylibpath(without trailing slash)

Note, no validity check is performed on the input dylibpath.


Note from 2017/1/28:

install_name_tool -add_rpath "/Users/qwang3/lib" `which chipmap2`

Then check with

otool -lv `which chipmap2` | l

Look for new LC_RPATH entry at the bottom
"""

import sys
from os.path import isfile



def change_dylib(target, oldlib, quiet=False):
    run_cmd(['chmod', '+w', target])
    run_cmd(['install_name_tool', '-change', oldlib,
             repl[oldlib.split('/')[-1]], target])
    if not quiet:
        run_cmd(['otool', '-L', target])


def check_libs_all():
    import os
    for file in os.listdir('.'):
        check_libs(file)


def check_libs(file):
    result = run_cmd(['otool', '-L', file], output=True, quiet=True)
    result = result.decode('utf-8').replace('\t', '').split('\n')
    touched = False
    if "not an object file" not in result[0]:
        for item in result[1:]:
            libname = item.split(' ')[0]
            if libname.split('/')[-1] in repl:
                change_dylib(file, libname, quiet=True)
                touched = True
            elif len(libname) > 0 and libname[0] != "/":
                print("%s not in request, but uses relative path!" % libname)
    if touched:
        run_cmd(['otool', '-L', file])


def run_cmd(cmd, quiet=False, output=None, dryrun=False, shell=False):
    '''
    Run shell command using subprocess.

    Input:
        cmd -- [(string)] command line arguments
        quiet -- don't print the command
        output -- whether to return the STDOUT
        dryrun -- don't actually send the command
        shell -- run the raw command in shell (expands *, ? etc.)

    Output:
        If output! = None, the STDOUT string is returned.
    '''
    import subprocess
    if not quiet:
        print(' '.join(cmd))
    if not dryrun:
        if output is not None:
            return subprocess.check_output(cmd, shell=shell)
        else:
            return subprocess.call(cmd, shell=shell)


if __name__ == '__main__':
    if not (len(sys.argv) == 3):
         print(__doc__)
         sys.exit(0)

    if not isfile(sys.argv[1]):
        print("File %s is not found." % sys.argv[1])
        sys.exit(1)

    dylib_path = sys.argv[2]

    repl = {
        "libcaldb4.3.dylib": dylib_path + "/libcaldb4.3.dylib",
        "libgrp.0.dylib": dylib_path + "/libgrp.0.dylib",
        "libregion.0.dylib": dylib_path + "/libregion.0.dylib",
        "libcxcparam.dylib": dylib_path + "/libcxcparam.dylib",
        "libpix.dylib": dylib_path + "/libpix.dylib",
        "libcfitsio.2.dylib": dylib_path + "/libcfitsio.2.dylib",
        "libcfitsio.8.dylib": dylib_path + "/libcfitsio.8.dylib",
        "libcfitsio.dylib": dylib_path + "/libcfitsio.dylib",
        "libascdm.dylib": dylib_path + "/libascdm.dylib",
        "libNewHdr2.dylib": dylib_path + "/libNewHdr2.dylib",
        "libds.dylib": dylib_path + "/libds.dylib",
        "libstk.dylib": dylib_path + "/libstk.dylib",
        "liberr.dylib": dylib_path + "/liberr.dylib",
        "libtcd.dylib": dylib_path + "/libtcd.dylib",
        "libpsf.dylib": dylib_path + "/libpsf.dylib",
        "libscs.dylib": dylib_path + "/libscs.dylib",
        "libtransform.dylib": dylib_path + "/libtransform.dylib",
        "libchips.dylib": dylib_path + "/libchips.dylib",
        "libcxcguiutils.dylib": dylib_path + "/libcxcguiutils.dylib",
        "libcxcguiipc.dylib": dylib_path + "/libcxcguiipc.dylib",
        "libreadline.dylib": dylib_path + "/libreadline.dylib",
        "libhistory.dylib": dylib_path + "/libhistory.dylib"
    }

    check_libs(sys.argv[1])
    sys.exit(0)
