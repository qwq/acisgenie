import sys
sys.path.append('..')

from unittest import TestCase

class TestCcdid_to_chipname(TestCase):
    def test_ccdid_to_chipname(self):
        from chandra.utils import ccdid_to_chipname
        # String input -- return False
        self.assertEqual(ccdid_to_chipname('1'), False)

        # Only 0 through 9 valid for ccd_id
        self.assertEqual(ccdid_to_chipname(0), 'i0')
        self.assertEqual(ccdid_to_chipname(9), 's5')
        self.assertEqual(ccdid_to_chipname(-1), False)
        self.assertEqual(ccdid_to_chipname(10), False)

        # List input gives same result as individual inputs
        arr = range(10)
        self.assertEqual(ccdid_to_chipname(arr), [ccdid_to_chipname(c) for c in arr])
