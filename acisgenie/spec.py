import astropy.io.fits as pf
import numpy as np
import subprocess
import time
import os
from .utils import run_cmd, require_file


def extract_spectrum(outname, regfile, obsinfo, exclregfile=None,
                     debugmode=False, do_corrfile=True, do_backfile=True):
    """
    Extract spectrum from region in regfile using files determined from
    obsinfo.

    Syntax:

    extract_spectrum('cluster4', 'reg/cluster4.reg', obsinfo,
        exclregfile='pts.reg')

    Manipulate obsinfo to specify what observations / chips to use. Will go
    into all specified obsids directories, extract spectra separately from
    each chip, then add the products and put them into an output directory.

    Required information from obsinfo:

    {
        "specparfile": "spec.par",
        "obs": [
            {
                "id": 1234,
                "chips": [2, 3, 5, 6, 7],
                "evtfiles": [
                    {
                        "chips": [2, 3, 6]
                    },
                    {
                        "chips": [5, 7]
                    }
                ],
                "bgfiles": [
                    {
                        "chips": [2, 3, 6]
                    },
                    {
                        "chips": [5, 7]
                    }
                ]
            }
        ]
    }


    Note on background scaling (BACKSCAL keyword) for XSPEC:

    http://heasarc.gsfc.nasa.gov/docs/asca/bgd_scale/bgd_scale.html

    The background flux scaling uses the BACKSCAL values of both the source
    spectrum and background spectrum; it is multiplied by the value of the
    source divided by the value of the background. extract_spectrum does not
    alter the BACKSCAL value of the source (leaves it at 1) and writes the
    BACKSCAL value of the background file only, which is 1/(rescaling ratio
    from calc_backscal).

    User beware when intepreting results from larger regions. In particular,
    one should check whether the effective exposure time varies significantly
    over the spectral extraction region, because the spectrum will be biased
    toward region with higher exposure time, as outlined below in the
    different usage scenarios. Additionally, it will also render the EXPOSURE
    value of the coadded spectrum inaccurate for flux determination. To
    mitigate, split larger region into subregions of similar effective
    exposure time.

    Usage scenarios:

    (1) Single observation, region contained in single CCD.

        The spectrum extraction procedure is performed one time. This is the
        lowest level building block of a coadded spectrum.

    (2) Single observation, region spans more than one CCD.

        Single-CCD spectra are extracted from each CCD that contains more than
        10 counts. The counts spectra (events, readoutbg, acisbg) are coadded.
        The EXPOSURE keyword of the coadded spectrum is the arithmetic
        average. ARF and RMF are weighed by 0.5-2 keV counts in the extraction
        region on each CCD.

        If the spectral extraction region straddles two CCDs that have very
        different exposure times (for example, adjacent FI and BI chips where
        one suffers a long flare and the other does not), this causes the
        emission from the CCD with the longer exposure to be over-represented.
        The exposure of the overall region is also undefined in this case.

    (3) Multiple observations, region spans more than one CCD, and FOV differs
       across the observations.

        Single-observation spectra are first created as described in (1) and
        (2). The counts spectra are coadded. The EXPOSURE keyword of the
        coadded spectrum is the arithmetic sum. ARF and RMF are weighed by
        0.5-2 keV counts in the extraction region in each observation.

        The same caveat applies as with (2). Further, if the FOV changes, then
        the true spectrum is weighed by the effective exposure over the
        extraction region, which will vary across the observations. If they
        are significantly different, such as when some observations only
        partly cover the extraction region, the result will be biased, and the
        exposure time is ill defined.
    """

    MSG_E_FILENOTFOUND = "Error: required file %s not found! Stopping"
    MSG_W_CHIPLOWCTS = (
        "WARNING: < raw 10 counts on chip %d of obsid %d, skipping.")
    MSG_E_RUNEXTRSPECFAIL = "Error: runextrspec did not execute completely"
    MSG_W_MVERR = "Warning: mv raised an error, but proceeding anyway"
    MSG_I_NOSPECFROMOBS = "No spectrum extracted from Obsid %d."

    SOFTCTSDMFILTER = "ccd_id=%d&&energy>500&&energy<2000&&sky=region(../%s)"

    from .utils import lookup_counts
    from .acisbg import calc_backscal
    from .wrap import chav_runextrspec, ftools_addrmf, ftools_addarf
    from .genie import getEventFileNames
    from .fits import update_keyword
    import os

    np.random.seed(int(time.time()))
    affix = np.random.randint(0, 10000)
    phafiles = {}
    rmffiles = {}
    arffiles = {}
    obslist = []  # List of obsids with spectra;
                  # not all obsids in non-overlapping observations may
                  # have any for a particular region.
    softcounts = {}  # 0.5-2 keV counts for weighing ARF & RMF files
    # bgexp = []  # track acisbg EXPOSURE keyword
    # bgbackscal = []  # track acisbg BACKSCAL keyword
    (evtlookup, gtilookup, readoutlookup, bglookup,
     aofflookup, softctslookup) = getEventFileNames(obsinfo)  # ,
    #    ['evt', 'gti', 'readout', 'acisbg', 'aoff'])
    parfile = obsinfo['specparfile']

    halt = False

    #######################################################################
    # Check data product files
    checklist = set()
    for _x in [
        evtlookup, gtilookup, readoutlookup, bglookup,
        aofflookup, softctslookup
    ]:
        for _y in _x:          # obsid
            for _z in _x[_y]:  # ccd_id
                checklist.add(str(_y) + '/' + _x[_y][_z])

    # spec.par file
    checklist.add(parfile)

    # spectral region file
    checklist.add(regfile)

    # point sources file
    if exclregfile:
        checklist.add(exclregfile)

    for _ in checklist:
        if not require_file(_, MSG_E_FILENOTFOUND % _):
            halt = True

    if halt:
        return False
    ###########################################################################

    # 1) Extract spectrum from chips for all obsid
    for obs in obsinfo['obs']:  # obs has evtfiles, bgfiles, chips, id
        # Loop over obsids-----------------------------------------------------
        obsid = obs['id']
        firstspec = True

        os.chdir("%d" % obsid)  # Work in the obs dir

        # Proceed with each chip separately
        for ccd_id in obs['chips']:

            # Loop over chips--------------------------------------------------
            evtfile = evtlookup[obsid][ccd_id]
            gtifile = gtilookup[obsid][ccd_id]
            aofffile = aofflookup[obsid][ccd_id]
            softctsfile = softctslookup[obsid][ccd_id]

            if do_corrfile:
                rofile = readoutlookup[obsid][ccd_id]
            else:
                rofile = None  # This will propagate down

            if do_backfile:
                bgfile = bglookup[obsid][ccd_id]
            else:
                bgfile = None

            tmpname = "tmp%d_%s_%d_%d" % (affix, outname, obsid, ccd_id)

            # ----------
            # Check that there are counts in extraction region on this chip,
            # skip if not!
            evt3counts = lookup_counts(
                evtfile, 'ccd_id=%d&&sky=region(../%s)' % (ccd_id, regfile)
            )
            print("%s ccd %d counts: %d" % (evtfile, ccd_id, evt3counts))
            if evt3counts < 10:
                print(MSG_W_CHIPLOWCTS % (ccd_id, obsid))
                continue  # skip to next ccd
            # ----------

            status = chav_runextrspec(
                tmpname, "../%s" % regfile, evtfile, gtifile, aofffile,
                '../%s' % parfile,
                bg_file=bgfile,
                readout_bg_file=rofile,
                chips=[ccd_id],
                exclregfile=(
                    '../%s' % exclregfile, None
                )[exclregfile is None]
            )
            if not status:
                print(MSG_E_RUNEXTRSPECFAIL)
                return False
            else:
                if firstspec:
                    phafiles['%d' % obsid] = []
                    rmffiles['%d' % obsid] = []
                    arffiles['%d' % obsid] = []
                    softcounts['%d' % obsid] = []
                    firstspec = False
                phafiles['%d' % obsid].append("%s.pha" % tmpname)
                rmffiles['%d' % obsid].append("%s.rmf" % tmpname)
                arffiles['%d' % obsid].append("%s.arf" % tmpname)
                softcounts['%d' % obsid].append(
                    lookup_counts(
                        softctsfile, SOFTCTSDMFILTER % (ccd_id, regfile)
                    )
                )

                # Write BACKSCAL into background spectrum file
                backscal = calc_backscal(evtfile, bgfile, [int(ccd_id)])
                update_keyword(1 / backscal, '%s.bg' % tmpname, 1, 'BACKSCAL')
            # / Loop over chips------------------------------------------------

        # Move products one directory level up
        try:
            run_cmd(['mv tmp%d_%s_%d_* ..' %
                     (affix, outname, obsid)], shell=True)
        except subprocess.CalledProcessError:
            print(MSG_W_MVERR)

        os.chdir('..')

        # ---------------------------------------------------------------------
        # Are there multiple chips?
        # Yes -- add across chips first
        # No -- rename files
        if '%d' % obsid not in phafiles:
            # No valid spectrum was extracted for this obsid
            print(MSG_I_NOSPECFROMOBS % obsid)
            continue  # Skip to next obsid

        elif len(phafiles['%d' % obsid]) > 1:
            add_spectra(
                '%s_%d' % (outname, obsid),
                phafiles['%d' % obsid],
                do_corrfile=do_corrfile,
                do_backfile=do_backfile,
                sameobs=True
            )
            totalcounts = np.sum(softcounts['%d' % obsid])
            weights = [1. * w / totalcounts for w in softcounts['%d' % obsid]]
            ftools_addarf('%s_%d.arf' % (outname, obsid),
                          arffiles['%d' % obsid], weights)
            ftools_addrmf('%s_%d.rmf' % (outname, obsid),
                          rmffiles['%d' % obsid], weights)
            obslist.append(obsid)

        else:  # if len(phafiles['%d' % obsid]) == 1:
            run_cmd(['mv %s %s_%d.pi' % (phafiles['%d' % obsid][0],
                                         outname, obsid)], shell=True)
            run_cmd(['mv %s %s_%d.rmf' % (rmffiles['%d' % obsid][0],
                                          outname, obsid)], shell=True)
            run_cmd(['mv %s %s_%d.arf' % (arffiles['%d' % obsid][0],
                                          outname, obsid)], shell=True)
            run_cmd(['mv %s %s_%d.ro' % (
                    phafiles['%d' % obsid][0].replace('.pha', '.rdt'),
                    outname, obsid)], shell=True)
            run_cmd(['mv %s %s_%d.bg' % (
                    phafiles['%d' % obsid][0].replace('.pha', '.bg'),
                    outname, obsid)], shell=True)

            # Need to update headers since add_spectra() isn't called.
            update_keyword(
                '%s_%d.bg' % (outname, obsid),
                '%s_%d.pi' % (outname, obsid),
                'SPECTRUM', 'BACKFILE')
            update_keyword(
                '%s_%d.ro' % (outname, obsid),
                '%s_%d.pi' % (outname, obsid),
                'SPECTRUM', 'CORRFILE')
            update_keyword(
                '%s_%d.arf' % (outname, obsid),
                '%s_%d.pi' % (outname, obsid),
                'SPECTRUM', 'ANCRFILE')
            update_keyword(
                '%s_%d.rmf' % (outname, obsid),
                '%s_%d.pi' % (outname, obsid),
                'SPECTRUM', 'RESPFILE')
            obslist.append(obsid)

        # / Loop over obsids---------------------------------------------------

    # 2) Add PHA files # Updated method - October 2015
    add_spectra(
        outname,
        ['%s_%d.pi' % (outname, obsid) for obsid in obslist],
        do_corrfile=do_corrfile,
        do_backfile=do_backfile
    )

    # 3) Add ARF and RMF files
    # Replacing in place
    softcounts = [np.sum(softcounts['%d' % obs]) for obs in obslist]
    totalsoftcounts = np.sum(softcounts)
    softweights = [1. * w / totalsoftcounts for w in softcounts]
    ftools_addarf(
        '%s.arf' % outname,
        ['%s_%d.arf' % (outname, obsid) for obsid in obslist],
        softweights
    )
    ftools_addrmf(
        '%s.rmf' % outname,
        ['%s_%d.rmf' % (outname, obsid) for obsid in obslist],
        softweights
    )

    # 5) Remove intermediate products
    if not debugmode:  # remove all temporary files
        run_cmd(['rm -rf tmp%s_%s_*_*' % (affix, outname)], shell=True)

    return True


def add_spectra(outname, filelist,
                sameobs=False, do_corrfile=True, do_backfile=True):
    """
    Add spectra and background if present in the CORRFILE and BACKFILE
    keywords (all input files must have the keyword set to enable this).
    CORRFILE should have EXPOSURE correctly scaled, and the BACKSCAL keyword
    in both the event file and background file should be appropriately set.
    The combined spectrum will have BACKSCAL of 1.0 in the main spectrum, and
    a suitable value set in the background spectrum.

    A more elaborate note on this is somewhere.

    Input:

    outname -- prefix of product file names
    filelist -- list of raw spectrum files
    sameobs -- whether to divide EXPOSURE of all output spectra
    by number of chips (default = False)
    """
    import datetime

    MSG_E_FILENOTFOUND = "Error: input file %s not found! Stopping"
    MSG_E_SPECEXT = "Error: expecting 1 SPECTRUM extension only in %s"
    MSG_E_CHANNELMISMATCH = (
        "Error: input file %s has mismatching channels with first file")
    MSG_E_ROWMISMATCH = (
        "Error: input file %s has mismatching row count with first file")
    MSG_E_REFNOTFOUND = (
        "Error: input file %s references %s=%s not found!")
    MSG_E_REFSPECEXT = (
        "Error: file %s referenced by %s has none "
        "or too many SPECTRUM extensions.")

    FITS_HDR = (
        "SIMPLE  =                    T / file does conform to FITS standard             "
        "BITPIX  =                  -32 / number of bits per data pixel                  "
        "NAXIS   =                    0 / number of data axes                            "
        "EXTEND  =                    T / FITS dataset may contain extensions            "
        "COMMENT   FITS (Flexible Image Transport System) format is defined in 'Astronomy"
        "COMMENT   and Astrophysics', volume 376, page 359; bibcode: 2001A&A...376..359H "
        "DATE    = '2015-07-24T07:44:10' / file creation date (YYYY-MM-DDThh:mm:ss UT)   "
        "CREATOR = 'Qian Wang'          / s/w task which wrote this dataset              "
        "QWVER   = '2'                  / s/w task version                               "
        "CONTENT = 'PHA SPECTRUM'       / SPECTRUM xtens (at least) present              ")

    import os

    print("Running add_spectra...")

    # Step 1. Checks on input files specified in filelist, and additional
    # files Arrange files by obsid, then an array of spectra from different
    # chips.
    rawflst = []
    corflst = []
    bgflst = []
    rawexp = 0.  # Simple sum of all inputs
    corexp = 0.
    bgexp = 0.

    if sameobs:             # Don't stack chip exptime from same obs
        expnorm = 1. / len(filelist)
    else:
        expnorm = 1.
    firstspec = True

    for specfile in filelist:

        dirname = os.path.dirname(specfile)
        if dirname == '':     # so that we have './samedirfile'
            dirname = '.'     # instead of '/samedirfile'
        if not os.path.isfile(specfile):  # Check file exists
            print(MSG_E_FILENOTFOUND % specfile)
            return False
        rawflst.append(pf.open(specfile))

        try:  # Check has only one SPECTRUM extension
            rawexp += rawflst[-1]['SPECTRUM'].header['EXPOSURE']
        except KeyError:
            print(MSG_E_SPECEXT % specfile)
            return False

        # Check that the channels match
        if firstspec:
            refspec = rawflst[0]
            firstspec = False
        else:
            if refspec['SPECTRUM'].header['NAXIS2'] == rawflst[-1]['SPECTRUM'].header['NAXIS2']:
                if np.sum(refspec['SPECTRUM'].data.field(0) != rawflst[-1]['SPECTRUM'].data.field(0)):
                    print(MSG_E_CHANNELMISMATCH % specfile)
                    return False
            else:
                print(MSG_E_ROWMISMATCH % specfile)
                return False

        corfname = rawflst[-1]['SPECTRUM'].header['CORRFILE']
        if do_corrfile and corfname != 'NONE':  # Validate corrfile
            if not os.path.isfile("%s/%s" % (dirname, corfname)):
                print(MSG_E_REFNOTFOUND % (specfile, "CORRFILE", corfname))
                return False
            corflst.append(pf.open("%s/%s" % (dirname, corfname)))
            try:  # Check CORRFILE has only one SPECTRUM extension
                corexp += corflst[-1]['SPECTRUM'].header['EXPOSURE']
            except KeyError:
                print(MSG_E_REFSPECEXT % (corfname, specfile))
                return False
        else:  # Disable corrfile adding
            do_corrfile = False

        bgfname = rawflst[-1]['SPECTRUM'].header['BACKFILE']
        if do_backfile and bgfname != 'NONE':  # Validate backfile
            if not os.path.isfile("%s/%s" % (dirname, bgfname)):
                print(MSG_E_REFNOTFOUND % (specfile, "BACKFILE", bgfname))
                return False
            bgflst.append(pf.open("%s/%s" % (dirname, bgfname)))
            try:    # Check BACKFILE has only one SPECTRUM extension
                bgexp += bgflst[-1]['SPECTRUM'].header['EXPOSURE']
            except KeyError:
                print(MSG_E_REFSPECEXT % (bgfname, specfile))
                return False
        else:  # Disable corrfile adding
            do_backfile = False

    # Step 2. Quick check on data conformity.
    # NOTE - skipped! Expecting all input files to be produced similarly and
    # as expected. This check is left to user!

    timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')
    hduhdr = pf.Header.fromstring(FITS_HDR)
    hduhdr["DATE"] = timestamp
    outfile = pf.HDUList([pf.PrimaryHDU(header=hduhdr)])

    # Step 3. Coadd raw spectra
    # Copy the bintable of the first input file to hold the results (sum
    # in-place) Simple '=' assignments do NOT copy the data array - only
    # copies the reference!
    outfile.append(coadd_spectra(rawflst))

    # Update keywords
    outfile['SPECTRUM'].header['EXPOSURE'] = rawexp * expnorm
    outfile['SPECTRUM'].header['BACKSCAL'] = 1.
    outfile['SPECTRUM'].header['CORRSCAL'] = 1.

    if do_backfile:
        outfile['SPECTRUM'].header['BACKFILE'] = "%s.bg" % outname
    else:
        outfile['SPECTRUM'].header['BACKFILE'] = 'NONE'
    if do_corrfile:
        outfile['SPECTRUM'].header['CORRFILE'] = "%s.ro" % outname
    else:
        outfile['SPECTRUM'].header['CORRFILE'] = 'NONE'
    outfile['SPECTRUM'].header['RESPFILE'] = '%s.rmf' % outname
    outfile['SPECTRUM'].header['ANCRFILE'] = '%s.arf' % outname
    outfile.writeto('%s.pi' % outname, overwrite=True)

    # Step 4. Coadd readout spectra
    if do_corrfile:
        outfile = pf.HDUList([pf.PrimaryHDU(header=hduhdr),
                              coadd_spectra(corflst)
                              ])

        # Update EXPOSURE keyword
        outfile['SPECTRUM'].header['EXPOSURE'] = corexp * expnorm
        # Leave other keywords alone, they're not used
        outfile.writeto('%s.ro' % outname, overwrite=True)

    # Step 5. Add weighted acis background spectra
    if do_backfile:
        outfile = pf.HDUList([
            pf.PrimaryHDU(header=hduhdr),
            pf.BinTableHDU(header=bgflst[0]['SPECTRUM'].header,
                           data=bgflst[0]['SPECTRUM'].data)
        ])

        # Refer to notes on adding exposure-time-area weighted background Note
        # float data type below different to coadding raw and readout spectra
        # Will need to round these back to int32 later
        orig_dtype = bgflst[0]['SPECTRUM'].data.field(1).dtype
        tmpsum = np.zeros(
            len(bgflst[0]['SPECTRUM'].data.field(1)), dtype=np.float64)

        rescale_exp_sum = 0.  # raw spec exp * raw spec backscal / bg backscal
        print('Adding ACIS background: background exposure, weight, weight tally')
        for i in range(len(bgflst)):
            rescale_exp = (
                1. * rawflst[i]['SPECTRUM'].header['EXPOSURE'] *
                rawflst[i]['SPECTRUM'].header['BACKSCAL'] /
                bgflst[i]['SPECTRUM'].header['BACKSCAL']
            )  # this is the weight
            rescale_exp_sum += rescale_exp
            tmpsum += (
                rescale_exp /
                bgflst[i]['SPECTRUM'].header['EXPOSURE'] *
                bgflst[i]['SPECTRUM'].data.field(1)
            )  # summing weighted background rates
            print(bgflst[i]['SPECTRUM'].header['EXPOSURE'],
                  rescale_exp, rescale_exp_sum)

        # Multiply by sum(background EXPOSURE), divide by normalization of the
        # weights, then multiply by 10 (reduce rounding after converting
        # back to int). Add 0.5 before recasting to int (numpy simply chops
        # off the floating part).
        tmpsum *= np.array(0.5 + bgexp / rescale_exp_sum * 10, dtype=np.int64)
        for i in range(len(tmpsum)):
            outfile['SPECTRUM'].data[i][1] = tmpsum[i]
        print('Total exposure: ', bgexp)

        # Update EXPOSURE keyword (also multiplying by 10)
        outfile['SPECTRUM'].header['EXPOSURE'] = 10 * bgexp * expnorm
        # Update BACKSCAL keyword here (since raw spectrum's BACKSCAL = 1.0)
        outfile['SPECTRUM'].header['BACKSCAL'] = 1. * rawexp / rescale_exp_sum
        print("Background BACKSCAL = %f" % (1. * rawexp / rescale_exp_sum))
        # Leave other keywords alone, they're not used
        outfile.writeto('%s.bg' % outname, overwrite=True)
    print('Done adding spectra.')
    return True


def update_backscal(infile, value):
    from .wrap import ciao_dmhedit, ciao_dmkeypar
    ciao_dmhedit("%s[SPECTRUM]" % infile, 'add', 'BACKSCAL', "%s" % value)
    print("New BACKSCAL: %f" % float(ciao_dmkeypar(infile, 'BACKSCAL')))


def coadd_spectra(spectra):
    """Coadd spectra BinTableHDU extension.

    Sum the counts column of BinTableHDU extensions.

    Arguments:
        spectra {[Fits containers]} -- List of Fits containers with 'SPECTRUM'
        extensions.

    Returns:
        BinTableHDU -- Containing the results.
    """
    bintablehdu = pf.BinTableHDU(header=spectra[0]['SPECTRUM'].header,
                                 data=spectra[0]['SPECTRUM'].data)
    coaddsum = 0 * spectra[0]['SPECTRUM'].data.field(1)  # Counts column
    for spec in spectra:
        coaddsum += spec['SPECTRUM'].data.field(1)
    for i in range(len(coaddsum)):
        bintablehdu.data[i][1] = coaddsum[i]
    return bintablehdu


def combine_spectra(filelist, outname):
    """
    Combine list of spectra files and associated readout, background, ARF, and
    RMF files.
    """
    from .wrap import ftools_addrmf, ftools_addarf

    proceed = True
    # Check that output files do not already exist
    outfiles = ['%s.%s' % (outname, _) for _ in ('pi', 'ro', 'bg', 'arf', 'rmf')]
    for _ in outfiles:
        if os.path.exists(_):
            proceed = False
            print('Output file %s exists.' % _)

    # Check input files exist
    for _ in filelist:
        if not os.path.exists(_):
            proceed = False
            print('Input file %s does not exist.' % _)

    if not proceed:
        print('Cannot proceed, stopping.')
        return False

    rofiles = []
    bgfiles = []
    arffiles = []
    rmffiles = []
    softweights = []

    for specfile in filelist:

        specfh = pf.open(specfile)
        spec_spectrum = specfh['SPECTRUM']  # store a reference
        rofile = spec_spectrum.header['CORRFILE']
        bgfile = spec_spectrum.header['BACKFILE']
        arffile = spec_spectrum.header['ANCRFILE']
        rmffile = spec_spectrum.header['RESPFILE']

        # Get energy grid from RMF file's EBOUNDS extension
        rmffh = pf.open(rmffile)
        rmf_ebounds = rmffh['EBOUNDS'].data  # store a reference

        # Check 0.5-2 keV counts
        chan_select = (rmf_ebounds.field(0) >= 0.5) * (rmf_ebounds.field(1) <= 2)
        softcounts = np.sum(spec_spectrum.data[chan_select].field(1))

        # Store in lists
        rofiles.append(rofile)
        bgfiles.append(bgfile)
        arffiles.append(arffile)
        rmffiles.append(rmffile)
        softweights.append(softcounts)

    ### Same function call as in extract_spectrum
    # 2) Add PHA files # Updated method - October 2015
    add_spectra(
        outname,
        filelist,
        do_corrfile=True,
        do_backfile=True
    )

    ### softcounts uses what's in the raw spectrum
    # 3) Add ARF and RMF files
    # Replacing in place
    softweights = np.float64(softweights)
    softweights /= np.sum(softweights)
    ftools_addarf(
        '%s.arf' % outname,
        arffiles,
        softweights
    )
    ftools_addrmf(
        '%s.rmf' % outname,
        rmffiles,
        softweights
    )

    return True

