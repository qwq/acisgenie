"""
FITS file manipulations
"""
import os
import re
import astropy.io.fits as pf


class FitsFile(object):

    def __init__(self, path):
        self.path = ''
        self.basename = ''
        self.properties = {}
        self.fits = self.open_file(path)

    def __del__(self):
        if 'fits' in self.__dict__:
            self.fits.close()

    def __getattr__(self, item):
        if item in self.properties:
            self.__dict__[item] = self.properties[item]()
        else:
            raise AttributeError
        return self.__dict__[item]

    def __repr__(self):
        msg = "Fits file at %s." % self.path
        for x in self.properties:
            if x in self.__dict__:
                msg = '\n'.join((msg, x, str(self.__dict__[x])))
        return msg

    def error(self, msg):
        print('Error: %s!' % msg)

    def open_file(self, path):
        """Returns PyFITS HDU list of the event file."""

        path, ext = parse_path(path)
        self.path = path
        self.open_ext = ext
        self.basename = os.path.basename(path)
        try:
            return pf.open(self.path, memmap=True)
        except FileNotFoundError:
            self.error('File %s not found' % self.path)
        except OSError as e:
            self.error('File %s could not be opened: %s' % (self.path, str(e)))
        return None

    def get_header(self, ext=1):
        """Get the FITS header of the first extension by default."""
        try:
            return FitsHeader(self.fits[ext].header)
        except IndexError:
            self.error("FITS extension %s not found" % str(ext))
            return None

    def get_extension(self, ext):
        """Fetch an extension of a FITS file if it exists."""
        try:
            return self.fits[ext]
        except IndexError:
            self.error("FITS extension %s not found" % str(ext))
            return None


class FitsHeader(object):

    def __init__(self, hdr):
        self.raw = hdr

    def get_key(self, key):
        """
        Return value of key or None if key does not exist. The header can also be accesssed directly as a dictionary to
        use different default values.
        """
        return self.raw.get(key, None)


"""
FITS write operations
"""

def update_keyword(value,filename,ext,keyword):
    """
    Update a header keyword.

    @param value: new value.
    @param filename: path to file to be updated.
    @type filename: string
    @param ext: FITS extension index
    @type ext: int or string
    @param keyword: header keyword to update
    @type keyword: string
    @returns: False if C{KeyError} is encountered
    """
    import astropy.io.fits as pf
    t = pf.open(filename)
    try:
        t[ext].header[keyword] = value
    except KeyError:
        print("update_keyword encountered KeyError!")
        return False
    t.writeto(filename, overwrite=True)
    return True


"""
GTI extension
"""
"""
Calculate exposure time from GTI
"""
def gti_exp(file, ext="GTI"):
    """
    Find the total GTI of a specified extension of the input file.

    @param file: path to file
    @type file: string
    @param ext: key of the extension
    @type ext: int or string
    @returns: (float) sum of the good time intervals in the GTI extension
    """
    from .eventfile import GtiExtension
    evtfits = FitsFile(file)
    gti = GtiExtension(evtfits.get_extension(ext))
    return gti.gti_exp()


"""
Counts and exposure time ratios
"""
def eventfile_energy_between(file, e1, e2, ccd=None):
    """
    Open an event file and find the number of events between energy e1 and e2.
    """
    evtfits = FitsFile(file)
    evts = evtfits.get_extension('EVENTS')
    return events_energy_between(evts, e1, e2, ccd=ccd)


def events_energy_between(evt, e1, e2, ccd=None):
    """
    Find number of photons between energies e1 and e2. Optionally specify
    C{ccd_id}.

    @param evt: the EVENTS extension
    @param e1: energy lower bound in eV
    @param e2: energy upper bound in eV
    @param ccd: C{ccd_id} to use
    @type ccd: [int]
    @returns: number of events between energy e1 and e2 [with specified
    C{ccd_id}].
    """
    import numpy as np
    mask = (evt.data.field('energy') > e1)*(evt.data.field('energy') < e2)
    if ccd is not None:
        chip_mask = np.zeros(len(evt.data), dtype=np.bool)
        for chip in ccd:
            chip_mask += (evt.data.field('ccd_id') == chip)
        mask *= chip_mask
    return np.sum(mask)


def parse_path(path):
    """
    Check if an extension shortcut is included at the end of the path, either
    of the form evt.fits[1] or evt.fits+1. They are common in CIAO and FTOOLS
    commands when a specific extension is intended, but are not understood by
    astropy.io.fits.

    Examples
    --------
    No extension specified ...
    Input: 'evt3.fits'
    Returns: ('evt3.fits', None)

    Extension specified using [] ...
    Input: 'evt3.fits[1]'
    Returns: ('evt3.fits', 1)

    Extension specified using + ...
    Input: 'clean.gti+2'
    Returns: ('clean.gti', 2)

    Bad input ...
    Input: 'evt3.fits[ 1]'
    Returns: ('evt3.fits[ 1]', None)
    """
    path_check1 = re.match(r'^(.*)\[(\d+?)\]$', path)
    path_check2 = re.match(r'^(.*)\+(\d+?)$', path)

    if path_check1 is not None:
        path = path_check1.group(1)
        ext = int(path_check1.group(2))
    elif path_check2 is not None:  # (they're mutually exclusive)
        path = path_check2.group(1)
        ext = int(path_check2.group(2))
    else:
        ext = None

    return (path, ext)
