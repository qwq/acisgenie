"""
Chandra data reduction tools
"""

__version__ = '1.0'

from . import conf
import importlib
importlib.import_module('%s.genie' % __name__)
importlib.import_module('%s.utils' % __name__)
importlib.import_module('%s.wrap' % __name__)


if not conf.check_caldb():
    print('Error: $CALDB environment variable is not set!')
    print('Please set $CALDB either in the shell before running Python,')
    print('or update os.environ[\'CALDB\'] with the appropriate value,')
    print('then run %s.conf.check_caldb().' % __name__)
