"""
Utility functions not specific to Chandra data processing.
"""


class Regfile:

    def __init__(self):
        self.header =  ''
        self.settings = ''
        self.coordinate_system = ''
        self.entries = []

    def __str__(self):
        text = 'This region file has coordinate system %s.\n' % self.coordinate_system
        text += 'It has %d entries.' % len(self.entries)
        return text


def run_cmd(cmd, quiet=False, output=None, dryrun=False, shell=False):
    """
    Run shell command using subprocess.

    @param cmd: command line arguments
    @type cmd: [(string)]
    @param quiet: don't print the command
    @type quiet: boolean
    @param output: whether to return the STDOUT
    @type output: boolean
    @param dryrun: don't actually send the command
    @type dryrun: boolean
    @param shell: run the raw command in shell (expands *, ? etc.)
    @type shell: boolean
    @return: the STDOUT string if output!=False.
    """
    import subprocess
    import os

    # Check if the env ACISGENIE_DYLIB_PATH is set; if so, prepend something to
    # the command. (For Macs to get around losing LD_LIBRARY_PATH through env)
    prepend = ''
    if 'ACISGENIE_DYLIB_PATH' in os.environ:
        prepend = 'LD_LIBRARY_PATH=%s ' % os.environ['ACISGENIE_DYLIB_PATH']

    if shell is True and isinstance(cmd, list):
        cmd = prepend + ' '.join(cmd)
    elif shell is False and isinstance(cmd, str):
        cmd = [prepend] + cmd.split(' ')

    if not quiet:
        if shell is False:
            print(' '.join(cmd))
        else:
            print(cmd)

    if not dryrun:
        if output is not False:
            return subprocess.check_output(cmd, shell=shell).decode('utf-8')
            # In 3.6, check_output has a new input encoding=
        else:
            return subprocess.call(cmd, shell=shell)


def require_file(filepath, errmsg=None, quiet=False):
    """
    Require a file to be present. Return True if path exists and is a file or
    valid symbolic link to a file (i.e. not directory). Print an error message
    if not found.

    @param filepath: file path(s) to check.
    @type filepath: string or [string]
    @param errmsg: a message to print if file does not exist.
    @type errmsg: string or None
    @param quiet: whether to print a message if file is not found.
    @type quiet: boolean
    @return: (boolean) whether file is found.
    """
    import os
    missing = False
    # When input is an array, recursively call itself
    if isinstance(filepath, list):
        for x in filepath:
            if not require_file(x):
                missing = True
    # Evaluate the filepath
    elif isinstance(filepath, str):
        if os.path.exists(filepath) and os.path.isfile(filepath):  # short-circuits
            return True
        else:
            if (not quiet) and (errmsg is not None):
                print(errmsg)
            return False
    # Close the array input case
    return not missing


def ccdid_to_chipname(chips):
    """
    Convert ccd_id (integer 0 to 9 inclusive) to chip name (i0 to i3, s0 to
    s5). E.g. ccd_id=3 is chip I3 and ccd_id=7 is chip S3.

    @param chips: ccd_id
    @type chips: int or [int]
    @returns: string or [string] containing corresponding CCD names.
    """
    if isinstance(chips, int):
        if 0 <= chips <= 9:
            return ['i0', 'i1', 'i2', 'i3', 's0', 's1', 's2', 's3', 's4', 's5'][chips]
        else:
            print("Error: expecting ccd_id between 0 and 9, got %d" % chips)
            return False
    elif isinstance(chips, list):
        return [ccdid_to_chipname(c) for c in chips]
    else:
        print("Error: expecting int or list input, got %s" % type(chips))
        return False


def lookup_counts(path, dmfilter):
    """
    Look up number of counts in C{EVENTS} extension using C{dmlist}.

    @param path: the event file.
    @type path: string
    @param dmfilter: additional filter for C{dmlist}.
    @type dmfilter: string
    @returns: (int) counts returned by C{dmlist} or False if C{dmlist}
    produces any other output.
    """
    from .wrap import ciao_dmlist
    from re import findall
    dmlistargs = ["%s[events][%s]" % (path, dmfilter), 'counts']
    output = ciao_dmlist(dmlistargs)
    rs = findall('\d+', output)
    if len(rs) == 1:
        counts = int(rs[0])
        return counts
    else:
        print("Error: expecting exactly one integer from dmlist, got %d" % len(rs))
        return False


def img_sobel_filter(image):
    """
    Applies Sobel filter to image.

    @param image: image to apply filter to
    @type image: numpy.ndarray
    @returns: (numpy.ndarray) filtered image
    """
    import scipy.ndimage
    import numpy
    dx = scipy.ndimage.sobel(image, 0)
    dy = scipy.ndimage.sobel(image, 1)
    mag = numpy.hypot(dx, dy)
    return mag


def read_regfile(regfile):
    """
    Open a region file and separate the header line (first comment line), the
    coordinate system line, and the region lines. Beware it only looks for
    lines that do not start with '#' for the regions.
    """
    with open(regfile) as fh:
        #print('Before', container)
        container = Regfile()
        #print('After', container)

        content = fh.read().splitlines()
        fh.close()

        # Quick check for the header line
        if content[0][:20] != '# Region file format':
            print('The file %s should begin with "# Region file format..."' % regfile)
            return False
        container.header = content[0]

        # Coordinate system
        coord_sys = ['physical', 'image', 'fk4', 'fk5',
                'icrs', 'galactic', 'ecliptic', 'wcs', 'wcsa', 'linear']
        coord_sys_maxlen = max(len(_) for _ in coord_sys)

        for line in content:

            # Skip empty lines and comment lines
            if line.strip() == '' or line[0] == '#':
                continue

            # Check for global settings line
            elif line[0:6] == 'global':
                container.settings = line

            elif len(line) <= coord_sys_maxlen and line in coord_sys:
                container.coordinate_system = line

            # The remaining stuff
            else:
                container.entries.append(line)

        if container.coordinate_system == '':
            print('Warning: no coordinate system found in file %s' % regfile)

        return container
