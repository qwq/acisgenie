"""
Helps process Chandra specific data. A Target class for a target object, and
functions that deal with Chandra specific data analysis steps.


Aligning multiple observations following the CIAO thread
========================================================

CIAO flux images are put in the obsid/align/ folder. wcs_match is used to
produce transform files that can be used with wcs_update to correct the aspect
solution file (asol) and the level=2 event file (evt2). The corrected evt2 and
asol files are put in obsid/processed/, to retain the originals in
obsid/primary/. The default FITS format transform files from wcs_match are
used; it has the necessary WCS information so that the original reference files
need not be kept, if reprocessing the evt2 file from archival data in the
future. Once the transform files are made the other by products in obsid/align/
are no longer needed.
"""
import astropy.io.fits as pf
import numpy as np
import os
import subprocess
import re
import time
import json
from .utils import run_cmd


class Target(object):

    def __init__(self, infofile=None):
        if infofile is not None:
            self.obsinfo = self.loadInfoFile(infofile)

    # TODO: these are inside the class, seems to be an error (used outside)
    ACISBG_PREFIX='acisbg'
    READOUT_PREFIX='readoutbg'
    EVT3_PREFIX='evt3'
    GTI_PREFIX='clean'
    AOFF_PREFIX='aoff1'
    obsinfo={}

    def saveInfoFile(self, file):
        pass

    def loadInfoFile(self, infofile, verbose=0):
        from acisgenie.genie import loadInfoJSON
        self.obsinfo = loadInfoJSON(infofile, verbose)

    def findInfo(self, path):
        print("Looking for obs files here:")
        run_cmd("pwd")
        evt1files=[]

        try:
            evt1str=subprocess.check_output(
                    ['ls -1 */secondary/*_evt1.fits*'],shell=True)
            evt1str=evt1str.split("\n")
            for item in evt1str:
                if len(item) > 0:
                    evt1files.append(item)
            print("Found %d evt1 files."%len(evt1files))

        except subprocess.CalledProcessError:
            print("No matching files found")

        print(evt1files)

        if len(evt1files) < 1:
            print("No event 1 files found, stopping")
            return

        # Does obsid.list exist? If yes, don't override. If so, create one.
        if (os.path.isfile('genie.fits')):
            print("Found genie.fits, loading previously saved information...")
        else:
            print("No genie.fits found, gathering information to make one...")
            for evt1 in evt1files:
                t=pf.open(evt1,memmap=True)
                print(t[1].header['OBS_ID'])
                print(t[1].header['DETNAM'])


class InfoFile:
    def __init__(self, json_import=None, verbose=1):
        self.properties = {}
        self._verbose = verbose
        self.import_json(json_import)

    def __repr__(self):
        """Print myself in JSON format"""
        return json.dumps(self.properties, indent=4)

    def __getattr__(self, item):
        if item in self.properties:
            self.__dict__[item] = self.properties[item]
        else:
            raise AttributeError
        return self.__dict__[item]

    def _print(self, *msg):
        if self._verbose > 0:
            print(*msg)

    def _errmsg(self, msg):
        # Distinguish from info messages
        print(msg)

    def get_object(self):
        """Return a copy of own properties"""
        return json.loads(json.dumps(self.properties))

    def import_json(self, json_import):
        """Import from JSON string"""
        if json_import is not None:
            self.import_obj(json.loads(json_import))

    def import_obj(self, obj):
        """Update own properties using matching entries found in input obj."""
        for key in self.properties:
            if key in obj:
                self.properties[key] = obj[key]

    def import_from_file(self, filename):
        """
        Import from a JSON file and return results of self.get_object() so
        that the following usage is possible, without separately instancing
        the object:

        obsinfo = InfoFile().import_from_file(jsonfile)
        """
        try:
            with open(filename) as fh:
                jsonstring = fh.read()
        except OSError:
            raise

        self.import_json(jsonstring)

        return self.get_object()

    def export_to_file(self, filename, overwrite=False):
        """
        Export to a JSON file. Possible one-liner for exporting the dictionary
        object:

        InfoFile(json_import=json.dumps(dict_obj)).export_to_file(filename)
        """
        if os.path.exists(filename) and not overwrite:
            self._errmsg('Error: %s exists and overwrite=False' % filename)
            return False

        with open(filename, 'w') as fh:
            fh.write(json.dumps(self.get_object(), indent=4))

        return True


class InfoEvtFile(InfoFile):
    def __init__(self, json_import=None, verbose=1):
        self.properties = {
            'aspectfile': '',  # aspect histogram from CHAV, used by convaspect
            'softctsfile': '',  # 0.5-2 keV point sources removed for spectral weights
            'gtifile': '',  # clean GTI
            'readoutfile': '',  # simulated readout events
            'aspfile': '',  # aspect histogram from CIAO, not the same as aspecthist
            'file': '',  # the events file itself
            'rorescale': 0.0, # rescaling factor for readoutbg, use updateReadoutbgImgRescale(obsinfo) to set automatically
            'aofffile': '',  # aspect offset file from CHAV
            'chips': []  # list of ints, one for each CCD ID (0-9) contained in this file
        }
        self._verbose = verbose
        self.import_json(json_import)

    def _validate_file(self):
        ok = True
        # Has valid file name
        if not isinstance(self.properties['file'], str):
            ok = False
            self._print("(X) ['file'] should be a string")
        elif self.properties['file'] == '':
            ok = False
            self._print("(X) ['file'] is unspecified (empty string)")
        return ok

    def _validate_chips(self):
        ok = True
        # Valid chips
        ccds = set()
        if not isinstance(self.properties['chips'], list):
            ok = False
            self._print("(X) ['chips'] must be a list, not ", type(self.properties['chips']))
        else:
            for _ in self.properties['chips']:
                if not isinstance(_, int):
                    ok = False
                    self._print('(X) ccd id type must be int: ', _, ' is type ', type(_))
                elif _ not in range(10):
                    ok = False
                    self._print('(X) invalid ccd id: ', _, ' (must be 0-9)')
                else:
                    ccds.add(_)
            if not (len(ccds) > 0):
                ok = False
                self._print('(X) No valid ccd id recorded')
        return ok

    def _validate_rorescale(self):
        # Has rescale factor filled
        if not self.properties['rorescale'] > 0.0:
            self._print('(X) rorescale not filled')
            return False
        else:
            return True

    def validate(self):
        ok = True
        self._print('Validating entry for %s' % self.properties['file'])

        if not self._validate_file():
            ok = False

        if not self._validate_chips():
            ok = False

        if not self._validate_rorescale():
            ok = False

        return ok


class InfoBgFile(InfoEvtFile):
    def __init__(self, json_import=None, verbose=1):
        self.properties = {
            'ctrescale': 0.0,  # rescaling factor for acisbg images, use updateAcisbgImgRescale(obsinfo) to set automatically
            'file': '',  # the acisbg events file itself
            'chips': []  # list of ints, one for each CCD ID (0-9) contained in this file
        }
        self._verbose = verbose
        self.import_json(json_import)

    def _validate_ctrescale(self):
        # Has rescale factor filled
        if not self.properties['ctrescale'] > 0.0:
            self._print('(X) ctrescale not filled')
            return False
        else:
            return True

    def validate(self):
        ok = True
        self._print('Validating entry for %s' % self.properties['file'])

        if not self._validate_file():
            ok = False

        if not self._validate_chips():
            ok = False

        if not self._validate_ctrescale():
            ok = False

        return ok


class InfoObsFiles(InfoFile):
    def __init__(self, json_import=None, verbose=1):
        self.properties = {
            'id': 0,  # obs id
            'folder': '',  # usually obs id, but can be anything to allow for different versions
            'chipmappar': 'chipmap.par',  # par file for chipmap2 from CHAV, default is just chipmap.par
            # -- datamode, bpix files filled by process_evt2
            'vfaint': True,  # from DATAMODE header keyword
            'bpixfits': '',  # FITS bad pixel file generated by CIAO
            'bpixtxt': '',  # text bad pixel file generated from the FITS file for chipmap2
            'evt2file': '',  # Level=2 events (not used)
            # -- chips, evtfiles to be filled by process_evt3
            'chips': [],  # list of ints, one for each CCD ID (0-9)
            'evtfiles': [],  # list of event file objects, one for each FITS file
            # -- bgfiles to be filled by process_acisbg
            'bgfiles': [],  # list of acis background event file objects, one for each FITS file
            # -- for processing archival files to Level=2, to be filled by lookup_obs_info()
            'evt1file': '',  # Level=1 events file
            'pbkfile': '',
            'mskfile': '',
            'statfile': '',
            'mtlfile': '',
            'asolfile': '',
            'usrbpixtxt': ''  # contains framestore shadow rows, created by make_framestore_bpixlist()
            # --
        }
        self._evt_obj = {}
        self._bg_obj = {}
        self._verbose = verbose
        self.import_json(json_import)

    def get_object(self):
        """Return a copy of own properties"""
        state = json.loads(json.dumps(self.properties))
        for filename in self._evt_obj:
            self._print(' - Adding evtfile %s' % filename)
            state['evtfiles'].append(self._evt_obj[filename].get_object())
        for filename in self._bg_obj:
            self._print(' - Adding bgfile %s' % filename)
            state['bgfiles'].append(self._bg_obj[filename].get_object())
        return state

    def update_evt(self, evt):
        # Replace reference having the same file
        self._evt_obj[evt.file] = evt

    def delete_evt(self, filename):
        # Remove reference for filename
        try:
            del self._evt_obj[filename]
        except KeyError:
            self._errmsg('Error: file %d not found.' % filename)

    def replace_evt(self, oldname, evt):
        # This is update and delete actions at the same time
        self.delete_evt(oldname)
        self.update_evt(evt)

    def update_bg(self, bg):
        # Replace reference having the same file
        self._bg_obj[bg.file] = bg

    def delete_bg(self, filename):
        # Remove reference for filename
        try:
            del self._bg_obj[filename]
        except KeyError:
            self._errmsg('Error: file %d not found.' % filename)

    def replace_bg(self, oldname, bg):
        # This is update and delete actions at the same time
        self.delete_bg(oldname)
        self.update_bg(bg)

    def import_obj(self, obj):
        # obj is a copy that was just created by json.loads()
        for key in self.properties:
            if key in obj:
                # For evtfiles and bgfiles, store them as InfoEvtFile and InfoBgFile objects
                if key == 'evtfiles':
                    for _ in obj[key]:
                        self.update_evt(InfoEvtFile(json_import=json.dumps(_), verbose=self._verbose))
                elif key == 'bgfiles':
                    for _ in obj[key]:
                        self.update_bg(InfoBgFile(json_import=json.dumps(_), verbose=self._verbose))
                else:
                    self.properties[key] = obj[key]

    def _validate_chips(self):
        ok = True
        # Valid chips
        ccds = set()
        if not isinstance(self.properties['chips'], list):
            ok = False
            self._print("(X) ['chips'] must be a list, not ", type(self.properties['chips']))
        else:
            for _ in self.properties['chips']:
                if not isinstance(_, int):
                    ok = False
                    self._print('(X) ccd id type must be int: ', _, ' is type ', type(_))
                elif _ not in range(10):
                    ok = False
                    self._print('(X) invalid ccd id: ', _, ' (must be 0-9)')
                else:
                    ccds.add(_)
            if not (len(ccds) > 0):
                ok = False
                self._print('(X) No valid ccd id recorded')
        return ok

    def _validate_evtfiles(self):
        ok = True

        # Check ._evt_obj
        if not isinstance(self._evt_obj, dict):
            self._print('._evt_obj must be type dict, found ', type(self._evt_obj))
            return False

        # Check that every ccd in chips is covered by evtfiles, and that there
        # is no overlap among them
        ccds = set()
        for evtkey in self._evt_obj:
            evtinfo = self._evt_obj[evtkey]

            # Check type
            if not isinstance(evtinfo, InfoEvtFile):
                self._print(evtkey, ' has unexpected type ', type(evtinfo))
                ok = False
            else:
                # Call validate of child
                if not evtinfo.validate():
                    ok = False

            # Check if file exists (do this in InfoObsFiles because it has the folder name)
            filepath = self.folder + '/' + evtinfo.properties['file']
            if not os.path.exists(filepath):
                self._print('File %s does not exist' % filepath)
                ok = False

            try:  # Wrap these in try in case .chips isn't valid
                for _ in evtinfo.chips:
                    if _ in ccds:
                        self._print('duplicate entry for ccd %d found: ' % (_, evtinfo.properties['file']))
                        ok = False
                    else:
                        ccds.add(_)
            except:  # Just flag as problematic without going further
                ok = False

        try:  # Wrap these in try in case .chips isn't valid
            if ccds != set(self.chips):
                self._print('listed ccds: ', str(set(self.chips)), ' but found data for ', str(ccds))
                ok = False
        except:
            ok = False

        return ok

    def _validate_bgfiles(self):
        ok = True

        # Check ._bg_obj
        if not isinstance(self._bg_obj, dict):
            self._print('._bg_obj must be type dict, found ', type(self._bg_obj))
            return False

        # Check that every ccd in chips is covered by bgfiles, and that there
        # is no overlap among them
        ccds = set()
        for bgkey in self._bg_obj:
            bginfo = self._bg_obj[bgkey]

            # Check type
            if not isinstance(bginfo, InfoBgFile):
                self._print(bgkey, ' has unexpected type ', type(bginfo))
                ok = False
            else:
                # Call validate of child
                if not bginfo.validate():
                    ok = False

            # Check if file exists (do this in InfoObsFiles because it has the folder name)
            filepath = self.folder + '/' + bginfo.properties['file']
            if not os.path.exists(filepath):
                self._print('File %s does not exist' % filepath)
                ok = False

            try:  # Wrap these in try in case .chips isn't valid
                for _ in bginfo.chips:
                    if _ in ccds:
                        self._print('duplicate entry for ccd %d found: ' % (_, bginfo.properties['file']))
                        ok = False
                    else:
                        ccds.add(_)
            except:  # Just flag as problematic without going further
                ok = False

        try:  # Wrap these in try in case .chips isn't valid
            if ccds != set(self.chips):
                self._print('listed ccds: ', str(set(self.chips)), ' but found data for ', str(ccds))
                ok = False
        except:
            ok = False

        return ok

    def validate(self):
        ok = True
        self._print('Validating entry for %d (%s)' % (self.properties['id'], self.properties['folder']))

        if not self._validate_chips():
            ok = False

        if not self._validate_evtfiles():
            ok = False

        if not self._validate_bgfiles():
            ok = False

        return ok


class InfoObsGroup(InfoFile):
    def __init__(self, json_import=None, verbose=1):
        self.properties = {
            'dirname': '',  # directory name (not used)
            'reg_skybg': '',  # (not used)
            'img_folder': 'images',
            'refobs': 0,  # images are projected onto the WCS grid of this obs id, usually the longest
            'reg_crop': 'reg/fk5.reg',  # crop region file for images
            'reg_lcclean': 'reg/lcclean.reg',  # (not used)
            'reg_pts': 'reg/pts.reg',  # point sources to be excluded (use positive regions)
            'target': '',  # name
            'img_subpix': 1,  # image binning
            'specparfile': 'spec.par',  # par file for runextrspec from CHAV
            'chipmapparfile': 'chipmap.par',  # chipmap2 par file
            'z': '',  # redshift -- used for making exposure map
            'nh': '',  # column N_H
            't': '',  # temperature in keV -- used for making exposure map
            'abund': '',  # abundance -- used for making exposure map
            'obs': []  # list of InfoObsFiles output objects
        }
        self._obs_obj = {}
        self._verbose = verbose
        self.import_json(json_import)

    def get_object(self):
        """Return a deep copy of own properties"""
        state = json.loads(json.dumps(self.properties))
        for obsid in self._obs_obj:
            self._print('Adding obsid %d' % obsid)
            state['obs'].append(self._obs_obj[obsid].get_object())
        return state

    def update_obs(self, obs, overwrite=False):
        if obs.id in self._obs_obj and overwrite is not True:
            self._errmsg('ObsID %d exists; use overwrite=True to replace.' % obs.id)
            return False
        else:
            # Replace the reference with new one
            self._obs_obj[obs.id] = obs
            return True

    def delete_obs(self, obsid):
        # Enable list input for removing many in one call. Useful for creating
        # subsets of available obsids from the 'complete' collection.
        if isinstance(obsid, list):
            for _ in obsid:
                self.delete_obs(_)

        # Delete a reference
        try:
            del self._obs_obj[obsid]
        except KeyError:
            self._errmsg('Error: obsid %d not found.' % obsid)

    def import_obj(self, obj):
        # obj is a copy that was just created by json.loads()
        for key in self.properties:
            if key in obj:
                if key == 'obs':
                    for _ in obj[key]:
                        self.update_obs(InfoObsFiles(json_import=json.dumps(_), verbose=self._verbose))
                else:
                    self.properties[key] = obj[key]

    def _validate_obs(self):
        ok = True

        # Check ._obs_obj
        if not isinstance(self._obs_obj, dict):
            self._print('._obs_obj must be type dict, found ', type(self._obs_obj))
            return False

        for obskey in self._obs_obj:
            obsinfo = self._obs_obj[obskey]

            if not obsinfo.validate():
                ok = False

        return ok

    def validate(self):
        ok = True
        self._print('Validating entries for ', self.target, ' in directory ', self.dirname)

        if not self._validate_obs():
            ok = False

        return ok

def loadInfoJSON(infofile, verbose=0):
    """
    Load observations info from JSON file into a dictionary object.
    """
    return InfoObsGroup().import_from_file(infofile)


def saveInfoJSON(data, infofile, overwrite=False):
    """
    Save observations info from dictionary object in a JSON file.
    """
    InfoObsGroup(json_import=json.dumps(data), verbose=0).export_to_file(infofile, overwrite=overwrite)


def updateAcisbgImgRescale(infoobs):
    """
    Calculate the rescale factor for ACIS background counts images.

    Arguments:
        infoobs -- InfoObsGroup object

    For each ACIS background event file, calculate the ratio of 9.5-12 keV
    counts vs. science event file, then compensate against the amount of
    background already included in the readout background. The input object
    is modified with the calculated values.
    """
    from .acisbg import calc_bgimg_rescale

    warn = False

    for obsid in infoobs._obs_obj:
        obs = infoobs._obs_obj[obsid]
        # ----------------
        print('Looking at OBSID %d' % obsid)
        print('-- evt3files:')
        for evtkey in obs._evt_obj:
            print('    ' + ''.join(map(str, obs._evt_obj[evtkey].chips)))
        print('-- bgfiles:')
        # ----------------
        for bgkey in obs._bg_obj:
            bginfo = obs._bg_obj[bgkey]
            # Find the event file that has the bg file chips as a subset
            # It is assumed that the event files are not arbitrarily divided,
            # but out of necessity due to different clean GTIs for FI and BI
            # chips. Thus, evt3 files are grouped into FI and BI sets.
            # Since ACIS background sets never contain both FI and BI chips
            # in the same file, there will be a unique evt3 file for each
            # ACIS background file. If none is found, then the files are
            # either missing or the obsinfo object is bad.
            refevt3 = None
            bgchips = set(bginfo.chips)
            for evtkey in obs._evt_obj:
                evtinfo = obs._evt_obj[evtkey]
                evt3chips = set(evtinfo.chips)
                if bgchips <= evt3chips:  # subset
                    common_chips = bgchips & evt3chips  # intersection
                    refevt3 = evtinfo.properties['file']
                    break

            if refevt3 is None:
                print('Error: could not find a reference evt3 file for '
                      'background with chips ' +
                      ''.join(map(str, bginfo.chips)))
                print('Either the files are incomplete, or the obsinfo object '
                      'is bad')
                warn = True
                continue   # Skip this background file

            # This rescaling already compensates for readout background
            rescale = calc_bgimg_rescale(obs.folder + '/' + refevt3,
                                         obs.folder + '/' + bginfo.properties['file'],
                                         ccd=common_chips)
            if rescale is not False:
                bginfo.properties['ctrescale'] = rescale
                print('    ' + ''.join(map(str, common_chips)) +
                      '\t' + str(rescale) + '\t' + obs.folder + '/' + bginfo.properties['file'])
            else:
                print('Error: something went wrong while calculating the '
                      'rescale factor. It was not updated.')
                warn = True
    if warn:
        print('Warning: not all steps were successful. Check error messages.')


def updateReadoutbgImgRescale(infoobs):
    """
    Update the readout background counts image rescale factor, which is based on
    the total exposure time per frame and the duration of the readout. The
    readout time is 0.04104 s, while the nominal exposure time is typically 3.1
    or 3.2 s. This information is taken from the header of the event files.

    Arguments:
        infoobs -- InfoObsGroup object
    """
    from .readoutbg import get_readout_rescale
    warn = False
    for obsid in infoobs._obs_obj:
        obs = infoobs._obs_obj[obsid]
        # ----------------
        print('Looking at OBSID %d' % obsid)
        print('-- evt3files:')
        for evtkey in obs._evt_obj:
            evtinfo = obs._evt_obj[evtkey]
            print('    ' + ''.join(map(str, evtinfo.chips)))
            # ----------------
            rescale = 1. / get_readout_rescale(obs.folder + '/' + evtinfo.properties['file'])

            if rescale is not False:
                evtinfo.properties['rorescale'] = rescale
                print("    " + str(rescale))
            else:
                print('Error: something went wrong while calculating the '
                      'rescale factor. It was not updated.')
                warn = True
    if warn:
        print('Warning: not all steps were successful. Check error messages.')


def fixReadoutbgGti(infoobs):
    """
    Append fake GTI extension to all readout background files in the
    InfoObsGroup object. Existing GTI extensions are stripped from the
    readout files so it is safe to run this on the same readout background
    files more than once.
    """
    from .readoutbg import readout_append_gti
    for okey in infoobs._obs_obj:
        ob = infoobs._obs_obj[okey]
        for ekey in ob._evt_obj:
            evt = ob._evt_obj[ekey]
            readout_append_gti(ob.folder + '/' + evt.properties['file'],
                               ob.folder + '/' + evt.properties['readoutfile'])


def getEventFileNames(obsinfo):
    """
    Find file names of event 3, acis bg, readout and gti files
    """

    MSG_I_LOOKUPFILES = "Determining filenames for ObsID %d..."
    MSG_W_EVT3OVERRIDE = (
        "Warning: file name override for event 3 "
        "but not specified for gti or readout file")

    evtlookup = {}
    gtilookup = {}
    readoutlookup = {}
    bglookup = {}
    aofflookup = {}
    softctslookup = {}

    for obs in obsinfo['obs']:  # obs has evtfiles, bgfiles, chips, id
        obsid = obs['id']
        evtlookup[obsid] = {}
        gtilookup[obsid] = {}
        readoutlookup[obsid] = {}
        bglookup[obsid] = {}
        aofflookup[obsid] = {}
        softctslookup[obsid] = {}

        print(MSG_I_LOOKUPFILES % obsid)

        # ---------------------------------------------------------------------
        # Work out which evt3, acisbg, readoutbg and clean.gti files each chip
        # uses. Based on the information listed under the "chips" keyword of
        # each file. Not expecting any chip to appear twice - if so, the most
        # recent find overrides. EVT3, READOUTBG, CLEAN.GTI Evt3, readout and
        # GTI files grouped by GTI (usually FI/BI separation) Because
        # runextrspec doesn't filter events by @gti, just the ASPOFF file so
        # need to have them already filtered.
        for a in obs['evtfiles']:
            if "file" in a:
                if "gtifile" not in a or "readoutfile" not in a:
                    print(MSG_W_EVT3OVERRIDE)
                b = a["file"]  # Override with specified file
                g = a["gtifile"]
                r = a["readoutfile"]
                aoff = a['aofffile']
                sc = a['softctsfile']
            else:
                suff = ''.join(str(s) for s in a['chips'])
                b = "%s_%s.fits" % (EVT3_PREFIX, suff)  # default naming,
                g = "%s_%s.gti" % (GTI_PREFIX, suff)   # e.g. evt3_01236.fits
                r = "%s_%s.fits" % (READOUT_PREFIX, suff)
                aoff = "%s_%s.fits" % (AOFF_PREFIX, suff)
                sc = "052_noso_%s.fits" % suff
            for c in a['chips']:
                evtlookup[obsid][c] = b
                gtilookup[obsid][c] = g
                readoutlookup[obsid][c] = r
                aofflookup[obsid][c] = aoff
                softctslookup[obsid][c] = sc
        # ACISBG
        for a in obs['bgfiles']:
            if "file" in a:
                b = a["file"]  # Override with specified file
            else:
                suff = ''.join(str(s) for s in a['chips'])
                b = "%s_%s.fits" % (ACISBG_PREFIX, suff)  # default naming,
            for c in a['chips']:                    # e.g. acisbg_01236.fits
                bglookup[obsid][c] = b
    return evtlookup, gtilookup, readoutlookup, bglookup, aofflookup, softctslookup


def make_flux_images(bands, skybin, obsinfo, regfile=None):
    """
    Make flux images

    Syntax:

    make_flux_images([[0.8,1.0],[1.0,2.0]],1,obsinfo)
    """

    MSG_E_SKYBIN = "Error: skybin >= 1 expected. Stopping..."
    MSG_E_ACISBGNOEVT3 = (
        "Error: could not find corresponding event 3 file for acisbg.")

    IMGDMFILTER = "[energy>%d&&energy<%d&&%s][bin sky=%d]"

    from .wrap import zhtools_gtiexp
    from .utils import ccdid_to_chipname
    from .acisbg import calc_backscal

    if skybin < 1:
        print(MSG_E_SKYBIN)
        return False

    np.random.seed(int(time.time()))
    refobs = obsinfo['refobs']
    if regfile is None:
        regfile = obsinfo['reg_crop']
    regfilter = 'sky=region(../%s)' % regfile
    TkeV = obsinfo['t']
    abund = obsinfo['abund']
    nH = obsinfo['nh']
    redshift = obsinfo['z']
    subpix = obsinfo['img_subpix']

    for band in bands:
        evt3imgs = []
        roimgs = []
        bgimgs = []
        expmaps = []
        emin = band[0]  # in keV
        emax = band[1]
        eminev = emin*1000  # for dmfilter in eV
        emaxev = emax*1000
        bandname = '%02d%02d' % (emin*10, emax*10)
        dmfilter = IMGDMFILTER % (eminev, emaxev, regfilter, skybin)
        # 1) Make counts images of evt3, acisbg, readoutbg files; make
        #    exposure maps
        for obs in obsinfo['obs']:  # obs has evtfiles, bgfiles, chips, id
            obsid = obs['id']
            # -----------------------------------------------------------------
            os.chdir("%d" % obsid)  # Work in the obs dir
            for evt3 in obs['evtfiles']:
                suffix = ''.join([str(s) for s in evt3['chips']])
                hdr = getHeader(evt3['file'])
                frametime = getFrametime(hdr)  # 5-CCD (3.1) or 6-CCD (3.2)?
                # counts image
                evt3outname = '%s_%d.img%s' % (bandname, skybin, suffix)
                run_cmd(['dmcopy',
                         '%s%s' % (evt3['file'], dmfilter),
                         '!%s' % evt3outname])
                evt3imgs.append('%d/%s' % (obsid, evt3outname))
                # readout
                outname = '%s_%d_ro.img%s' % (bandname, skybin, suffix)
                run_cmd(['dmcopy',
                         '%s%s' % (evt3['readoutfile'], dmfilter),
                         '!%s' % outname])
                readoutrescale = zhtools_gtiexp(evt3['file']) / zhtools_gtiexp(evt3['readoutfile'])
                run_cmd(['imarith', outname, '=', outname, '*', str(readoutrescale)])
                roimgs.append('%d/%s' % (obsid, outname))

                # exposure map
                chipstring = ','.join(ccdid_to_chipname(evt3['chips']))
                outname = '%s_%d.expmap%s' % (bandname, skybin, suffix)
                run_cmd(['chipmap2',
                         '-emin', str(emin),
                         '-emax', str(emax),
                         '-evtfile', evt3['file'],
                         '-o', 'jnk',
                         '-bin', str(skybin),
                         '-chips', chipstring,
                         '-t', str(TkeV),
                         '-abund', str(abund),
                         '-nh', str(nH),
                         '-z', str(redshift),
                         '@../%s' % obs['chipmappar']])
                run_cmd(['convaspect', '-chipmap', 'jnk',
                         '-asphist', evt3['aspectfile'], '-o', '!jnk1'])
                # NOTE: chipmap2 uses an image of the aspect produced by CHAV's
                # aspecthist. This is NOT compatible with CIAO's asphist product!
                run_cmd(['projectimg',
                         'from=jnk1',
                         'onto=%s' % evt3outname,
                         'out=%s' % outname,
                         'exact=yes',
                         'subpix=%d' % subpix])
                if regfile is not None:
                    run_cmd(['dmcopy', '%s[%s]' % (outname, regfilter), '!%s' % outname])
                else:
                    run_cmd(['mv', 'jnk1', outname])
                expmaps.append('%d/%s' % (obsid, outname))
            for acisbg in obs['bgfiles']:
                suffix = ''.join([str(s) for s in acisbg['chips']])
                # find corresponding evt3 file name
                evt3f = None
                for evt3 in obs['evtfiles']:
                    if evt3['chips'].count(acisbg['chips'][0]) > 0:  # this is
                        evt3f = evt3['file']                            # the one
                if evt3f is None:
                    print(MSG_E_ACISBGNOEVT3)
                    return False
                # acis bg
                outname = '%s_%d_bg.img%s' % (bandname, skybin, suffix)
                run_cmd(['dmcopy',
                         '%s%s' % (acisbg['file'], dmfilter),
                         '!%s' % outname])
                bgrescale = (zhtools_gtiexp(evt3f) /
                             zhtools_gtiexp(acisbg['file']) *
                             calc_backscal(evt3f, acisbg['file'], acisbg['chips']) *
                             frametime/(frametime+0.04104))  # account for readout
                run_cmd(['imarith', outname, '=', outname, '*', str(bgrescale)])
                bgimgs.append('%d/%s' % (obsid, outname))
            os.chdir("..")  # Go back up
            # -----------------------------------------------------------------
        # 2) Project images onto the refobs counts image
        for img in evt3imgs:
            if re.match('^%d/\d+_\d+.img\d*' % refobs, img) is not None:
                refimg = img
                break
        for img in evt3imgs:
            if re.match('^%d/\d+_\d+.img\d*' % refobs, img) is not None:
                continue  # no need to reproject the refobs images
            else:
                run_cmd(['projectimg',
                         'from=%s' % img,
                         'onto=%s' % refimg,
                         'out=%s' % img,   # overwrite in place
                         'exact=yes', 'subpix=%d' % subpix])
        for img in roimgs:
            if re.match('^%d/\d+_\d+_ro.img\d*' % refobs, img) is not None:
                continue  # no need to reproject the refobs images
            else:
                run_cmd(['projectimg',
                         'from=%s' % img,
                         'onto=%s' % refimg,
                         'out=%s' % img,   # overwrite in place
                         'exact=yes', 'subpix=%d' % subpix])
        for img in bgimgs:
            if re.match('^%d/\d+_\d+_bg.img\d*' % refobs, img) is not None:
                continue  # no need to reproject the refobs images
            else:
                run_cmd(['projectimg',
                         'from=%s' % img,
                         'onto=%s' % refimg,
                         'out=%s' % img,   # overwrite in place
                         'exact=yes', 'subpix=%d' % subpix])
        for img in expmaps:
            run_cmd(['projectimg',
                     'from=%s' % img,
                     'onto=%s' % refimg,
                     'out=%s' % img,   # overwrite in place
                     'exact=yes', 'subpix=%d' % subpix])
        # 3) Sum images per band
        cmd = ['addimages']
        cmd.extend(evt3imgs)
        cmd.append('%s_%d.img' % (bandname, skybin))
        if run_cmd(cmd):  # evt3 counts image
            rmcmd = ['rm', '-rf']
            rmcmd.extend(evt3imgs)
            run_cmd(rmcmd)  # remove intermediate files
        cmd = ['addimages']
        cmd.extend(roimgs)
        cmd.append('%s_%d_ro.img' % (bandname, skybin))
        if run_cmd(cmd):  # readout counts image
            rmcmd = ['rm', '-rf']
            rmcmd.extend(roimgs)
            run_cmd(rmcmd)
        cmd = ['addimages']
        cmd.extend(bgimgs)
        cmd.append('%s_%d_bg.img' % (bandname, skybin))
        if run_cmd(cmd):  # acis background counts image
            rmcmd = ['rm', '-rf']
            rmcmd.extend(bgimgs)
            run_cmd(rmcmd)
        cmd = ['addimages']
        cmd.extend(expmaps)
        cmd.append('%s_%d.expmap' % (bandname, skybin))
        if run_cmd(cmd):  # exposure map image
            rmcmd = ['rm', '-rf']
            rmcmd.extend(expmaps)
            run_cmd(rmcmd)
        # Flux image
        cmd = ['imarith', 'jnk', '=', '%s_%d.img' % (bandname, skybin), '-',
               '%s_%d_bg.img' % (bandname, skybin)]
        run_cmd(cmd)
        cmd = ['imarith', 'jnk', '=', 'jnk', '-', '%s_%d_ro.img' % (bandname, skybin)]
        run_cmd(cmd)
        cmd = ['imarith', 'jnk', '=', 'jnk', '/', '%s_%d.expmap' % (bandname, skybin)]
        run_cmd(cmd)
        cmd = ['mv', 'jnk', '%s_%d.flux' % (bandname, skybin)]
        run_cmd(cmd)
    return True


def script_process_evt2(infoobs, readoutonly=False):
    """
    Writes a bash script that will process Level=1 archival data to evt2.fits.
    Run this first on all archival data.

    Following instructions
    http://cxc.harvard.edu/ciao/threads/createL2/index.html Using CIAO 4.7 and
    CALDB 4.6.8 at time of writing (July 2015).

    See inline comments for details about each step (including note about
    setting bitflags for acis_build_badpix and impact on later event filtering
    and exposure map mask.

    @param obs: InfoObsFiles object (from C{lookup_obs_info})
    """

    obs = infoobs.properties
    cwd = os.getcwd()  # Bookmark this to return to later

    os.chdir(infoobs.folder)  # Work in the obsid directory

    def _write_process_evt2(output):
        filepath = "process_evt2"
        with open(filepath, 'w') as fh:
            fh.write(output)
        print('Written %d/%s' % (obsid, filepath))
        os.chmod(filepath, 0o755)

    outtxt = ['#!/bin/bash']

    # --- information already in InfoObsFiles from lookup_obs_info()
    obsid = obs['id']
    evt1file = obs['evt1file']
    pbkfile = obs['pbkfile']
    usrbpixtxt = obs['usrbpixtxt']
    mskfile = obs['mskfile']
    statfile = obs['statfile']
    asolfile = obs['asolfile']
    mtlfile = obs['mtlfile']
    # ---

    # Intermediate products
    destreakfile = 'secondary/evt1_destreak.fits'
    evt1parfile = 'secondary/%d_evt1.par' % obsid
    biaslistfile = 'secondary/bias.lis'
    abb1file = 'secondary/abb1_bpix1.fits'
    aglowfile = 'secondary/aglow_bpix1.fits'
    bpixfile = 'secondary/%d_bpix1.fits' % obsid
    evt1outfile = 'secondary/%d_evt1.fits' % obsid
    evt2outfile = 'primary/%d_evt2.fits' % obsid
    readoutfile = 'primary/%d_readoutbg.fits' % obsid
    tmpreadoutfile = 'primary/pre_readout.fits'
    asphistfile = 'primary/%d.asphist' % obsid
    fovfile = 'primary/fov.reg'
    fk5file = 'primary/fk5.reg'
    rawgtifile = 'primary/raw.gti'
    bpixtxtfile = bpixfile.replace('bpix1.fits', 'bpix1.txt')

    # --------------------------------------------------------------
    # Taking this out of here, in case the folder isn't named after the obsid (e.g. user renamed it)
    # In those cases download_chandra_obsid would not put the missing files in the right folder anyway
#     outtxt += ['cd ..']
#     outtxt += [' '.join(['download_chandra_obsid', '%d' % obsid,
#                'evt1,mtl,asol,bias,pbk,stat,msk,bpix'])]
#     outtxt += ['cd %s' % infoobs.folder]
    # --------------------------------------------------------------
    # outtxt += ' '.join(['cd', '%d/secondary'%obsid])
    outtxt += [' '.join(['acis_clear_status_bits', '%s' % evt1file])]
    outtxt += [' '.join(['destreak', 'infile=%s' % evt1file, 'outfile=%s' % destreakfile,
               'mask=None', 'filter=no'])]
    # Default looks at ccd_id=8 only
    # Flagged events filtered by [status=xxxxxxxxxxxxxxxx1xxxxxxxxxxxxxxx]
    outtxt += [' '.join(['dmmakepar', destreakfile, evt1parfile, 'clob+'])]
    outtxt += [' '.join(['cd', 'secondary'])]
    outtxt += [' '.join(['ls', '-1', 'acisf*_?_bias*.fits*', '>', os.path.basename(biaslistfile)])]
    outtxt += [' '.join(['cd', '..'])]

    # For 'usrfile=' use a text file specifying the framestore shadow rows
    # (this will be added)

    # For 'bitflag=' use a filter to remove from the list (i.e. not remove
    # events) certain regions

    # bitflags: 00000000000000020021100020022222
    # (Default, CALDB + obs specific)

    #           00000000000000011111100010012111
    # (Only include neighbours for bias=4095 (dead) pixels, these are flagged
    # with event status bit 6 by acis_format_events. This seems to be the only
    # way to include these in the bpix1.fits file so that exposure maps have
    # the same mask using BPMASK=0x1F99F)

    outtxt += [' '.join(['acis_build_badpix', 'obsfile=%s' % evt1parfile,
                         'pbkfile=%s' % pbkfile, 'biasfile=@%s' % biaslistfile,
                         'outfile=\!%s' % abb1file, 'usrfile=%s' % usrbpixtxt,
                         'bitflag=00000000000000011111100010012111',
                         'calibfile=CALDB', 'mode=h', 'verbose=0'])]
    outtxt += [' '.join(['acis_find_afterglow', 'infile=%s' % destreakfile,
                         'outfile=\!%s' % aglowfile, 'badpixfile=%s' % abb1file,
                         'maskfile=%s' % mskfile, 'statfile=%s' % statfile, 'mode=h',
                         'verbose=0'])]
    # Run again to process flagged afterglow events & order pixels correctly
    outtxt += [' '.join(['acis_build_badpix', 'obsfile=%s' % evt1parfile,
                         'pbkfile=%s' % pbkfile, 'biasfile=None', 'outfile=\!%s' % bpixfile,
                         'bitflag=00000000000000011111100010012111',
                         'calibfile=%s' % aglowfile, 'mode=h', 'verbose=0'])]
    outtxt += [' '.join(['dmhedit', destreakfile, 'none', 'add', 'BPIXFILE',
                         os.path.basename(bpixfile)])]
    outtxt += [' '.join(['dmmakepar', destreakfile, evt1parfile, 'clob+'])]

    # DATAMODE -- FAINT/VFAINT -- whether to set check_vf_pha=yes
    _ = pf.open(evt1file)
    datamode = _[1].header['DATAMODE']
    if datamode == 'VFAINT':
        check_vf_pha = 'yes'
    else:
        check_vf_pha = 'no'

    if not readoutonly:
        outtxt += [' '.join(['acis_process_events', 'infile=%s' % destreakfile,
                             'outfile=\!%s' % evt1outfile, 'badpixfile=%s' % bpixfile,
                             'acaofffile=%s' % asolfile, 'mtlfile=%s' % mtlfile,
                             'eventdef=")stdlev1"', 'check_vf_pha=%s' % check_vf_pha])]

    # Also create the readout background file from the destreaked file
    outtxt += [' '.join(['make_readout_bg', destreakfile, tmpreadoutfile, asolfile,
                         bpixfile, '0', 'CALDB', 'CALDB', mtlfile])]

    # Just to make sure
    if not readoutonly:
        outtxt += [' '.join(['r4_header_update', evt1outfile])]

    outtxt += [' '.join(['r4_header_update', tmpreadoutfile])]

    # Create a text file from the bpix1 FITS file.
    outtxt += [' '.join(['bpix_fits_to_txt', bpixfile, bpixtxtfile])]

    # Apply standard grade and status filters.

    # The status filter ignores columns that are flagged as being adjacent to
    # bad columns, which would otherwise be applied as default for VFAINT
    # mode.
    if not readoutonly:
        outtxt += [' '.join(['dmcopy', '%s"[EVENTS][grade=0,2,3,4,6,status=b00000000000000000000000000x00000]"'%evt1outfile,
            '\!%s'%evt2outfile])]
    outtxt += [' '.join(['dmcopy', '%s"[EVENTS][grade=0,2,3,4,6,status=b00000000000000000000000000x00000]"'%tmpreadoutfile,
        '\!%s'%readoutfile])]

    # Remove the phas column, not needed (these are required by acis_process_events)
    if not readoutonly:
        outtxt += [' '.join(['dmcopy', '%s"[cols -phas]"'%evt2outfile,'\!%s'%evt2outfile])]
    outtxt += [' '.join(['dmcopy', '%s"[cols -phas]"'%readoutfile,'\!%s'%readoutfile])]

    #outtxt += ' '.join(['cd', '..'])
    if not readoutonly:
        # Obtain FoV regions (physical coords)
        outtxt += [' '.join(['skyfov', evt2outfile, fovfile, 'aspect=%s'%asolfile, 'mskfile=%s'%mskfile,
            'kernel=ascii', 'clob+'])]
        # Extract raw GTI extension
        outtxt += [' '.join(['fextract', '%s"[gti]"'%evt2outfile, rawgtifile])]
        # Create aspect histogram (GTI not yet applied)
        outtxt += [' '.join(['asphist', asolfile, asphistfile, evt2outfile])]
        # Make quick look images
        outtxt += [' '.join(['dmcopy', '%s"[energy>500&&energy<4000&&sky=region(%s)][bin sky=1]"'%(
            evt2outfile, fovfile), '\!0540.img'])]
        # Make FOV region in DS9/fk5 format/coords
        outtxt += ['ds9 0540.img -regions load %s '
                   '-regions format ds9 -regions system wcs -regions sky fk5 '
                   '-regions skyformat sexagesimal -regions save %s '
                   '-exit \n' % (fovfile, fk5file)]

    _write_process_evt2('\n'.join(outtxt))

    os.chdir(cwd)
    #------- updates for InfoObsFiles ------------
    infoobs.properties['bpixfits'] = bpixfile
    infoobs.properties['bpixtxt'] = bpixtxtfile
    infoobs.properties['vfaint'] = (datamode == 'VFAINT')
    #---------------------------------------------


def script_process_evt3(infoobs):
    """
    Write a shell script, a parameter file for lc_clean, and lcclean.reg
    template that will be used to process evt2.fits to evt3.fits. Run this
    after script_process_evt2(). If BI chips are present, an additional
    parameter file *_BI.par will be created to treat them apart from the FI
    chips. lc_clean will need to be run once using that parameter file. There
    will also be two evt3.fits, separately for FI and BI events. Note that
    lc_clean uses lc_clean.par by default, but can also take a user supplied
    file name in #1 argument.

    Before running this function:

    1) A region file containing point sources (as positive regions) should be
       created and saved as reg/pts.reg. The 0.5-4 keV counts images from the
       Level=2 processing step can be coadded to make an image for point
       sources inspection.

    2) A region file with positive region(s) masking the main cluster emission
       should be created and saved as reg/lcclean_template.reg. This will be
       used to generate a preliminary %obsid%/primary/lcclean.reg, to which
       further masking along chip y columns can be added if needed.

    Parameters
    ----------
    infoobs - InfoObsFiles object
    """
    import os
    from .eventfile import EventFile
    from .utils import require_file, read_regfile

    obs = infoobs.properties
    cwd = os.getcwd()  # Bookmark this to return to later

    os.chdir(infoobs.folder)  # Work in the obsid directory

    obsid = obs['id']
    asolfile = os.path.basename(obs['asolfile'])

    checklist = set()
    checklist.add('primary/%s' % asolfile)
    checklist.add('primary/fk5.reg')
    checklist.add('../reg/pts.reg')
    checklist.add('../reg/lcclean_template.reg')

    halt = False
    for x in checklist:
        if not require_file(x, "Required file %s not found"%x):
            halt = True

    if halt:
        return False

    def _write_lcclean_par(bi=False, use_ratio=False):

        if bi:
            chip_filter = "ccd_id>4&&ccd_id<8&&ccd_id!=6"
            bi_suffix = "_BI"
        else:
            chip_filter = "ccd_id!=5&&ccd_id!=7"
            bi_suffix = ""

        if use_ratio:
            ratio_option = ""
        else:
            ratio_option = "#"

        filepath = 'primary/lc_clean%s.par' % bi_suffix

        #-- Do not overwrite existing par file that user may have modified
        #   To get a fresh par file, remove the existing one.
        if os.path.exists(filepath):
            print('File %s exists; remove it to get a fresh one.' % filepath)
            return

        with open(filepath, "w") as fh:
            lcclean_content = """## lc_clean.par
## A leading space comments the line out; all lines except in the form
## name=value are ignored.

# First specify event files (can use the standard FITSIO filtering syntax).
# There may be several of them (e.g., a split observation);
# if they are not time-ordered, provide a parameter tbeg (in Chandra seconds)
# to specify the beginning of the observation.
#
# To clean an ordinary light curve, just use file*; to clean by flux ratio
# in two energy bands, for each file* set fileref* (the denominator)
# which must be the same evt. file with a different energy filter (using
# FITSIO syntax).

num_datafiles=1

file1={obsid}_evt2.fits[events][regfilter('lcclean.reg')&&energy>2300&&energy<7300&&{filter}]
{ratio}fileref1={obsid}_evt2.fits[events][regfilter('lcclean.reg')&&energy>9500&&energy<12000&&{filter}]

binsize=1037.12
#binsize=2000

# Input and output GTI filenames:
gti_file=raw.gti               - input uncleaned GTI file
clean_gti_file=clean{suffix}.gti      - (optional) output cleaned GTI file
lc_dirty=raw?{suffix}.lc                   - (optional) files for lightcurves
lc_clean=clean{suffix}.lc


# The following is used to calculate the mean rate around which to clean.
# Set either of the two:

clip=3              - this many sigmas to clip to calc. mean
 mean=0.441     - use this preset mean value instead of computing

# The following is used to identify which lightcurve bins are bad.
# Set either max_factor or max_sigma:

max_factor=1.2         - max increase/decrease factor from the mean
 max_sigma=2           - max dev. from the mean rate, sigmas (NOT RECOMMENDED)

 ignore_neg_dev=y      - ignore negative dev. from mean (normally excluded)

 ignore_spikes=y       - ignore positive dev. from mean (normally excluded)
 ignore_nodata=y       - ignore bins with 0 counts (normally excluded)

# Output options:
 write_table_lc=y   - a simple table instead of a histogram
 write_separate_chips=y - write uncleaned lc for each acis chip
""".format(
            obsid=obsid,
            filter=chip_filter,
            suffix=bi_suffix,
            ratio=ratio_option
    )
            fh.write(lcclean_content)
            print("Written to file %s"%filepath)

    def _write_process_evt3(bi=False):
        if bi:
            chip_filter = "ccd_id=5,7"
            bi_suffix = "_BI"
        else:
            chip_filter = "ccd_id!=5&&ccd_id!=7"
            bi_suffix = ""

        filepath = 'process_evt3%s' % bi_suffix
        with open(filepath, 'w') as fh:
            script_content = """#!/bin/bash

cd primary

lc_clean lc_clean{suffix}.par

cd ..

mkdir processed

dmcopy primary/{obsid}_evt2.fits'[@primary/clean{suffix}.gti][{filter}]' \!processed/evt3{suffix}.fits
dmcopy primary/{obsid}_readoutbg.fits'[@primary/clean{suffix}.gti][{filter}]' \!processed/readoutbg{suffix}.fits
dmcopy primary/{asolfile}'[@primary/clean{suffix}.gti]' \!processed/asol1{suffix}.fits
# CIAO aspect histogram file
asphist processed/asol1{suffix}.fits \!processed/asphist{suffix}.fits processed/evt3{suffix}.fits
# A.V.'s aoff1 and aspecthist (an image) files
asol2aoff -o \!processed/aoff1{suffix}.fits -evtfile processed/evt3{suffix}.fits -asol1 processed/asol1{suffix}.fits
aspecthist aspfile=processed/aoff1{suffix}.fits -o \!processed/aspecthist{suffix}.fits -gtifile primary/clean{suffix}.gti

ln -s ../primary/clean{suffix}.gti processed/clean{suffix}.gti

# Generate lightcurve plot
plot_lc primary/clean{suffix}.lc primary/rawacis{suffix}.lc

# Make soft counts file
dmcopy processed/evt3{suffix}.fits'[energy>500&&energy<2000]' \!jnk052
dmcopy jnk052'[exclude sky=region(../reg/pts.reg)]' \!processed/052_noso{suffix}.fits
rm -f jnk052
""".format(
    filter=chip_filter,
    suffix=bi_suffix,
    obsid=obsid,
    asolfile=asolfile
    )
            fh.write(script_content)
            print("Written to file %d/%s" % (obsid, filepath))
            os.chmod(filepath, 0o755)

        # --- create InfoEvtFile object
        infoevt = InfoEvtFile(json_import=json.dumps({
            'file': 'processed/evt3%s.fits' % bi_suffix,
            'aspectfile': 'processed/aspecthist%s.fits' % bi_suffix,
            'gtifile': 'processed/clean%s.gti' % bi_suffix,
            'readoutfile': 'processed/readoutbg%s.fits' % bi_suffix,
            'aspfile': 'processed/asphist%s.fits' % bi_suffix,
            'aofffile': 'processed/aoff1%s.fits' % bi_suffix,
            'softctsfile': 'processed/052_noso%s.fits' % bi_suffix,
            'chips': []  # To be added
            }))
        # ---
        return infoevt

    def _write_lcclean_reg():
        """
        Combine the regions in fov.reg (include), lcclean_template.reg
        (exclude), and pts.reg(exclude) to create a preliminary lcclean.reg.
        fov.reg contains the regions covered by the CCDs, lcclean_template.reg
        contains regions of extended emission that should be masked, and
        pts.reg contains the point sources. While this may be good to use
        as-is, it should be loaded in DS9 overlaying a counts image of only
        that obsid, and checked for additional columns to mask because of
        readout artifacts from very bright sources. If there is any present,
        they should be added to lcclean.reg.
        """
        regfilepath = 'primary/lcclean.reg'

        #-- Do not overwrite existing reg file that user may have modified
        #   To get a fresh reg file, remove the existing one.
        if os.path.exists(regfilepath):
            print('File %s exists; remove it to get a fresh one.' % regfilepath)
            return

        with open(regfilepath, 'w') as fh:
            ERRMSG_LCCLEAN = "Error: failed to create lcclean.reg!"
            templ = """# Region file format: DS9 version 4.1
global color=green
fk5
"""
            # Add inclusion regions first; grab the polygon regions from fk5.reg
            regs1 = read_regfile('primary/fk5.reg')
            if not regs1:
                print(ERRMSG_LCCLEAN)
                return False
            if len(regs1.entries) == 0:
                print('Error: the FOV region file cannot be empty.')
                return False
            for _ in regs1.entries:
                templ += '%s\n' % _
            #print(regs1)
            # Add exclusion regions, extended ones first (make excl if not
            # already, by prefixing with a '-' if the first character isn't
            # '-')
            regs2 = read_regfile('../reg/lcclean_template.reg')
            if not regs2:
                print(ERRMSG_LCCLEAN)
                return False
            for _ in regs2.entries:
                templ += '%s%s\n' % (('-', '')[_[0] == '-'], _)
            #print(regs1, regs2)
            # Add exclusion regions for point sources
            regs3 = read_regfile('../reg/pts.reg')
            if not regs3:
                print(ERRMSG_LCCLEAN)
                return False
            for _ in regs3.entries:
                templ += '%s%s\n' % (('-', '')[_[0] == '-'], _)
            #print(regs1, regs2, regs3)
            fh.write(templ)
            fh.close()
        print("Written to file %s" % regfilepath)

    def _write_view_lcclean_regs():
        """
        Just a helper scripts to pop DS9 and check the lc_clean regions.
        """
        filepath = 'view_lcclean_regs.sh'
        script_content = """#!/bin/bash
ds9 0540.img -region load primary/lcclean.reg -block 32 -zoom 8
"""
        with open(filepath, 'w') as fh:
            fh.write(script_content)
        os.chmod(filepath, 0o755)
        print("Written to file %s" % filepath)


    # Look for event 2 file and check if it contains chip 5 or 7:
    has_bi = False
    evtfits = EventFile('primary/%d_evt2.fits' % obsid)
    chips = evtfits.get_chips()
    if (5 in chips) or (7 in chips):
        has_bi = True

    _write_lcclean_par()
    fievtinfo = _write_process_evt3()
    fievtinfo.properties['chips'] = [_ for _ in chips if (_ != 5 and _ != 7)]

    if has_bi:
        _write_lcclean_par(bi=True)
        bievtinfo = _write_process_evt3(bi=True)
        bievtinfo.properties['chips'] = [_ for _ in chips if (_ == 5 or _ == 7)]

    _write_lcclean_reg()
    _write_view_lcclean_regs()

    os.chdir(cwd)
    #------- updates for InfoObsFiles ------------
    infoobs.properties['chips'] = list(chips)  # make a copy
    infoobs.update_evt(fievtinfo)  # Note: if the InfoObsFiles object isn't new, this
    if has_bi:                     # won't remove existing entries that don't have the same name
        infoobs.update_evt(bievtinfo)  # Best not to recycle that when reducing the data
    #---------------------------------------------

    return True


def script_process_readoutbg(infoobs):
    """
    Writes a bash script that uses evt3.fits to make read out background event
    file. Run this after script_process_evt3().

    make_readout_bg takes the original Level=1 file, scrambles chipy values,
    and runs acis_process_events on these repositioned events. The resulting
    file should be treated the same way as the Level=2 file. Use the same GTI
    filter as for evt3.fits. Finally, amend the EXPOSURE so that it scales
    correctly. Readout takes 0.04104 s, while frame time is 3.1 s for 5-CCD
    observation and 3.2 s for 6-CCD observations. (This information can be
    found in the TIMEDEL keyword in the header.)

    Note -- updated script_process_evt2 file will create a readoutbg.fits file
    with evt2.fits. The evt2.fits file does not have PHA column so
    make_readout_bg cannot use it. See procedure in script_process_evt2.

    @param obs: observation info object (from C{lookup_obs_info})
    """
    script_process_evt2(infoobs, readoutonly=True)
    return


def script_process_acisbg(infoobs):
    """
    Generate a script that will process the ACIS background events files for a
    given cleaned events file, to be run in the obsid folder (due to the use
    of relative paths in several places, this is not flexible). Run this
    after script_process_evt3().

    Input - InfoObsFiles object
    """

    import caldb4
    from .eventfile import EventFile
    from .fits import parse_path

    def echo_run(cmd):
        """Echo the command being run (so that they are included in logs)"""
        return 'echo "%s"\n%s' % (cmd.replace('"', '\\"'), cmd)

    def _write_process_acisbg(output):
        filepath = scriptfile
        with open(filepath, 'w') as fh:
            fh.write(output)
        os.chmod(filepath, 0o755)
        print("Written to file %d/%s" % (obsid, filepath))


    obs = infoobs.properties
    obsid = obs['id']

    cwd = os.getcwd()  # Bookmark this to return to later

    os.chdir(infoobs.folder)  # Work in the obsid directory

    # ---

    scriptfile = 'process_acisbg'

    make_acisbg_cmd = 'make_acisbg evt_file={evt_file} aoff1_file={aoff1_file} bg_file={bg_file} gti_file={gti_file} out={out}'
    vfaint_cmd = 'dmcopy "{infile}[status=0]" "{outfile}" option=all'
    badpixfilter_cmd = 'badpixfilter evtfile={evtfile} o=!{o} -badpixfilter {bpix_txt}'
    fappend_cmd = 'fappend {orig}+2 {dest}'

    script = ['#!/bin/bash\n']

    ccds_done = set()

    # # --- Check for existing entries for acisbg
    # WIP
    # if skip_existing:
    #     for bgkey in infoobs._bg_obj:
    #         bginfo = infoobs._bg_obj[bgkey]
    #         for _ in bginfo.properties['chips']:
    #             ccds_done.add(_)

    # --- Run this loop for each evt3 file found in the InfoObsFiles object
    #     Note that the look-up is only performed for the first time a CCD is
    #     encountered. There is no reason data for the same chip should be in
    #     more than one events file for the same observation.

    for evtkey in infoobs._evt_obj:

        evtinfo = infoobs._evt_obj[evtkey]  # (key is the file name)

        evtfile = evtinfo.properties['file']
        bpixfile = infoobs.bpixtxt
        aofffile = evtinfo.aofffile
        gtifile = evtinfo.gtifile

        # The CALDB query for ACIS background files, taken from acis_bkgrnd_lookup
        cdb = caldb4.Caldb(telescope='CHANDRA', instrume='ACIS', product='BKGRND', infile=evtfile)

        print('Looking for background files for %d (%s)' % (obsid, evtkey))
        acisbgpaths = {}
        for ccd in evtinfo.chips:
            if ccd in ccds_done:
                print('Skipping %d' % ccd)
                continue   # skip
            acisbgpaths[ccd] = None  # Default result if no matching file is found.
            cdb.CCD_ID = ccd
            match = cdb.search
            if len(match) == 0:
                print('Error: no background file found for ccd %d! Skipped' % ccd)
                continue
            elif len(match) > 1:
                print('Warning: %d background files found for ccd %d, using the first result' % (len(match), ccd))
            else:
                print('Found bg file for %d' % ccd)
            acisbgpaths[ccd] = parse_path(match[0])[0]
            ccds_done.add(ccd)  # Add to done list

        # Print some readable information containing the context here, to make it easier
        # to trace problems with the background

        script.append('# Generating script for processing acis background files for:')
        script.append('# evt_file=%s' % evtfile)
        script.append('# CCDs: %s' % ''.join(str(_) for _ in evtinfo.chips))
        if infoobs.vfaint == True:
            script.append('# An additional filter [status=0] will be applied for VFAINT mode data.')
        script.append('# badpixfilter=%s' % bpixfile)
        script.append('# This should be the bad pixel list that was generated with the events file,')
        script.append('# and the same bad pixel list that will be used to create the exposure map.')
        script.append('# The following blank sky background files were found in CALDB:')
        script.append('# CCD\tFile')
        for ccd in acisbgpaths:
            script.append('# %d\t%s' % (ccd, str(acisbgpaths[ccd])))
        script.append('\n\n')

        for ccd in acisbgpaths:
            script.append('echo "-------------------------------------"')
            if acisbgpaths[ccd] is None:
                script.append('echo "Skipping CCD %d, no blank sky background file found."' % ccd)
            else:
                script.append('echo "Processing CCD %d"' % ccd)
                tmpfile = 'jnk_acisbg%d.fits' % ccd
                outfile = 'processed/acisbg%d.fits' % ccd

                script.append(echo_run(make_acisbg_cmd.format(evt_file=evtfile, aoff1_file=aofffile,
                                             bg_file=acisbgpaths[ccd], out=tmpfile, gti_file=gtifile)))
                if infoobs.vfaint == True:
                    script.append('echo "DATAMODE = VFAINT so applying filter, overwrite file in-place"')
                    script.append(echo_run(vfaint_cmd.format(infile=tmpfile, outfile='\\!%s' % tmpfile)))  # Overwrite
                    script.append('echo "dmcopy loses the GTI extension, need to add it back."')
                    script.append(echo_run(fappend_cmd.format(orig=acisbgpaths[ccd], dest=tmpfile)))

                script.append('echo "Apply same bad pixel mask as the events file"')
                script.append(echo_run(badpixfilter_cmd.format(evtfile=tmpfile, o=outfile, bpix_txt=bpixfile)))

                script.append(echo_run('rm -f %s' % tmpfile))

                #------- updates for InfoObsFiles ------------
                bginfo = InfoBgFile(json_import='{"file": "%s"}' % outfile)
                bginfo.properties['chips'] = [int(ccd)]
                infoobs.update_bg(bginfo)
                #---------------------------------------------

            script.append('echo "-------------------------------------"')
            script.append('\n')

        script.append('')

    _write_process_acisbg('\n'.join(script))

    os.chdir(cwd)


def lookup_obs_info(obsid, override=None):
    """
    Looks up file information required to reprocess Level=1 archival data into
    Level=2 products. If this is run after Level=2 products have been created,
    for instance to get information for processing Level=2 file further, there
    may be more than one `*_evt1.fits`, or possibly none, if they have been
    removed. In that case, use the `override={'evt1file':evt2file}` optional
    parameter to use the Level=2 file instead. This is OK, the event file is
    only used to determine which CCDs are in use and to give an
    informational print on the exposure time.

    Info for script_process_evt2:
    id, evt1file, pbkfile, mskfile, statfile, asolfile, mtlfile, usrbpixtxt

    Optional parameter override -- provide a dict to specify certain files.
    Use this to specify `evt1file`. For example,

    lookup_obs_info(4215, override={'evt1file': '4215/primary/evt2.fits'})

    Run this in the folder where the obsid folders are kept.
    """
    from glob import glob
    import os
    from .fits import gti_exp
    from .eventfile import EventFile

    obsid = int(obsid)  # In case it is input as string -- make it int as reference

    cwd = os.getcwd()  # Remember this to return later

    os.chdir('%d' % obsid)  # Go into obsid directory to record relative paths

    MSG_E_FILENOTFOUND = "No file found for %s!"
    MSG_E_FILEMULTI = "Multiple files found for %s!"

    fail = False

    obs = {}
    obs['id'] = obsid
    obs['folder'] = str(obsid)

    archivelist = {'evt1file': 'secondary/acisf*evt1.fits.gz',
        'pbkfile': 'secondary/acisf*N???_pbk0.fits*',
        'mskfile': 'secondary/acisf*N???_msk1.fits*',
        'statfile': 'secondary/acisf*N???_stat1.fits*',
        'mtlfile': 'secondary/acisf*N???_mtl1.fits*',
        'asolfile': 'primary/pcadf*N???_asol1.fits*'}
    for f in archivelist:
        if override != None and f in override:
            obs[f] = override[f]
            continue
        match = glob(archivelist[f])
        if len(match) == 0:
            print(MSG_E_FILENOTFOUND%f)
            fail = True
        elif len(match) > 1:
            print(MSG_E_FILEMULTI%f)
            fail = True
        else:
            obs[f] = match[0]

    if fail:
        os.chdir(cwd)
        return False

    # Framestore bad pixel list
    evt1 = EventFile(obs['evt1file'])
    usrbpixfile = 'secondary/badpix_framestore_%s.txt' % (''.join([str(s) for s in evt1.chips]))
    if not os.path.isfile(usrbpixfile):
        make_framestore_bpixlist(evt1.chips, usrbpixfile)
    obs['usrbpixtxt'] = usrbpixfile
    print("Exposure: %d s (Level=1)" % gti_exp(obs['evt1file']))

    os.chdir(cwd)

    return InfoObsFiles(json_import=json.dumps(obs))


def make_framestore_bpixlist(chips,filepath):
    """
    Creates a text based list of pixels covering the Framestoreore pixels, for
    use as usrbpixfile in acis_build_badpix.
    """
    fh=open(filepath,'w')
    fh.write('#ccd_id chipx_lo chipx_hi chipy_lo chipy_hi time time_stop bit action')
    fh.write('\n#------ -------- -------- -------- -------- ---- --------- --- ------')
    for ccd_id in chips:
        fh.write('\n%d       2       1023    2       9       0       0       1       include'%ccd_id)
    fh.close()


def make_instmap_weights(bands, outprefix, instmapfunc, pars):
    """
    Batch make instmap weights using acisgenie.image.instmap_weights_apec.

    >>> bands=[[0.8,1.0],
            [1.0,1.5],
            [1.5,2.0],
            [2.0,3.0],
            [3.0,4.0],
            [4.0,5.0],
            [5.0,6.0],
            [6.0,7.3],
            [7.6,9.0]]
    >>> pars=[7.9,0.0898,0.088,0.223]
    >>> make_instmap_weights(bands, 'weights/', instmap_weights_apec, pars)
    """
    for b in bands:
        bandname='%02d%02d'%(b[0]*10,b[1]*10)
        instmapfunc("%s%s.wt"%(outprefix,bandname),pars,b[0],b[1],0.02)
    return True

"""
ls -1 ?.expmap
dmimgcalc '?.expmap' infile2= out=0520_1.expmap \
           op="imgout=img1+img2+img3+img4+img5" clob+
dmhedit 0520_1.expmap none add DETNAM "ACIS-23567"
"""
def batch_expmap(maskfile, spectrumfiledir, spectrumfiles, chips, evtfile,
    asphistfile, badpixfile, outprefix, xygridimg, dryrun=False):
    """
    Batch run make_expmap() to make exposure maps for one event file, in many
    energy bands. The separate exposure maps for individual chips are added
    using dmimgcalc. Output file has name (outprefix)(weightprefix).expmap.

    Parameters:

    maskfile -- typically msk1 file
    spectrumfiledir -- the folder to find weight files
    spectrumfiles -- list of weight files; output files will have the
    same prefix
    chips -- the ccd_id of chips to process
    evtfile -- the reference file to make expmap for, evt3.fits
    asphistfile -- aspect histogram file, typically asphist.fits
    badpixfile -- use BPIXFILE value in the event file (relative path)
    outprefix -- prefix of output files
    xygridimg -- the reference image grid
    (typically an image of the longest exposure)
    dryrun -- whether to do a dry run

    Example:
    >>> batch_expmap('15186/secondary/acisf15186_000N002_msk1.fits.gz',
    'expmap/weights', ['0810.wt','1015.wt','1520.wt','2030.wt','3040.wt',
    '4050.wt','5060.wt','6073.wt','7690.wt'], [5,7],
    '15186/processed/evt3_57.fits', '15186/processed/asphist_57.fits',
    '15186/secondary/15186_bpix1.fits','expmap/15186', '15186/0520_1.img')
    """
    from .image import make_expmap
    nchip = len(chips)
    imgop = 'imgout='+'+'.join(['img'+str(s) for s in np.arange(len(chips))+1])
    detnamval = 'ACIS-%s'%(''.join([str(s) for s in chips]))
    for sf in spectrumfiles:
        bandname = sf.replace('.wt','')
        make_expmap('%s/%s'%(spectrumfiledir,sf), chips, evtfile,
                asphistfile, badpixfile, outprefix, xygridimg,
                maskfile=maskfile, dryrun=dryrun)
        outfilename = '%s%s.expmap'%(outprefix,bandname)
        run_cmd(['dmimgcalc','%s?.expmap'%outprefix,
                        'infile2=',
                        'out=%s'%outfilename,
                        'op=%s'%imgop,
                        'clob+'],dryrun=dryrun)
        run_cmd(['dmhedit', outfilename, 'none', 'add', 'DETNAM', detnamval],
                                                            dryrun=dryrun)
        run_cmd(['rm','%s_?.expmap'%outprefix,'%s_?.instmap'%outprefix],
                                                            dryrun=dryrun)
        print("Done %s."%outfilename)
    return True

"""
acisgenie.genie.batch_expmap('9424/secondary/acisf09424_000N002_msk1.fits.gz',
'weights', ['0870.wt'], [0,1,2,3,6], '9424/primary/9424_evt2.fits',
'9424/primary/9424.asphist', '9424/secondary/9424_bpix1.fits',
'expmap/9424_', )
"""
