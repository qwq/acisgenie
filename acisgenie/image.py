"""
Convenience functions for making flux images.
"""
import subprocess
import numpy as np
from .utils import run_cmd

def instmap_weights_apec(outfile,pars,emin,emax,ewidth):
    """
    Create instrument map weights for making exposure maps using CIAO
    (make_instmap_weights).

    Model: wabs * apec

    Parameters:

    [0]kt -- temperature / keV
    [1]z -- redshift
    [2]nh -- absorption column / 10^20 cm^-2
    [3]abund -- relative to solar abundance
    emin -- band lower energy / keV
    emax -- band upper energy / keV
    ewidth -- energy bin size when calculating weights / keV
    """
    cmd=['make_instmap_weights',outfile,
        "xswabs.abs1*xsapec.apec1",
        "paramvals=abs1.nh=%f;apec1.redshift=%f;apec1.kt=%f;apec1.abundanc=%f"%(pars[2],pars[1],pars[0],pars[3]),
        "emin=%f"%emin,"emax=%f"%emax,"ewidth=%f"%ewidth,"clobber=yes"]
    run_cmd(cmd)
    return True


def make_expmap(chips, evtfile, asphistfile, badpixfile,
        outprefix, xygridimg, monoenergy=1.0,
        spectrumfile="NONE", maskfile="NONE", dryrun=False):
    """
    Make exposure maps following CIAO 4.7 instructions. First, instrument maps
    are created for each chip separately. Then, exposure maps are created from
    these. The resulting exposure maps have outputname?.expmap, where ? is the
    ccd_id of chips.
    """
    run_cmd(['punlearn', 'ardlib'],dryrun=dryrun)
    run_cmd(['acis_set_ardlib', badpixfile],dryrun=dryrun)
    run_cmd(['punlearn','mkinstmap'],dryrun=dryrun)
    run_cmd(['pset','mkinstmap',
                    'pixelgrid=1:1024:#1024,1:1024:#1024',
                    'maskfile=%s'%maskfile,
                    ('spectrumfile=%s'%spectrumfile,
                        'monoenergy=%f'%monoenergy)[spectrumfile=="NONE"],
                    'obsfile=%s'%evtfile,
                    'mode=h'],dryrun=dryrun)
    for c in chips:
        run_cmd(['mkinstmap',
                        'detsubsys=ACIS-%d;BPMASK=0x1F99F'%c,
                        #'mirror=HRMA;AREA=1',
                        'outfile=%s%d.instmap'%(outprefix,c),
                        'clob+'],dryrun=dryrun)
    run_cmd(['get_sky_limits',xygridimg],dryrun=dryrun)
    run_cmd(['pset','mkexpmap','xygrid=)get_sky_limits.xygrid',
                    'normalize=no', 'mode=h', 'verbose=2',
                    'asphistfile=%s'%asphistfile],dryrun=dryrun)
    for c in chips:
        run_cmd(['mkexpmap','instmapfile=%s%d.instmap'%(outprefix,c),
            'outfile=%s%d.expmap'%(outprefix,c), 'clob+'],dryrun=dryrun)
    return True


def make_counts_img(regfiles,evtfiles,bands,skybin,outfiles):
    for b in bands:
        bandname='%02d%02d'%(b[0]*10,b[1]*10)
        for i in np.arange(len(evtfiles)):
            regfile=regfiles[i]
            evt=evtfiles[i]
            cmd=['dmcopy','%s[sky=region(%s)&&energy>%d&&energy<%d][bin sky=%d]'%(evt,regfile,b[0]*1000,b[1]*1000,skybin),
                '!%s'%(outfiles[i].replace('.img','_%s.img'%bandname))]
            print (' '.join(cmd))
            subprocess.call(cmd)


def merge_expmaps(bands,prefix,matchfile,outprefix):
    """
    Merge exposure maps (e.g. for FI and BI chips) using CIAO's
    reproject_image, resolution=2 and method=average.

    Example:
    >>> merge_expmaps([[0.8,1.0]], ['5005_','15186_FI_','15186_BI_'], 'reproject_image_target.img','')
    """
    for b in bands:
        bandname='%02d%02d'%(b[0]*10,b[1]*10)
        infiles=','.join(['%s%s.expmap'%(pre,bandname) for pre in prefix])
        cmd=['reproject_image', infiles, 'matchfile=%s'%matchfile,
            'outfile=%s%s.expmap'%(outprefix,bandname), 'resolution=2', 'method=average', 'verbose=3', 'clob+']
        print (' '.join(cmd))
        subprocess.call(cmd)

def merge_counts_img(bands,prefix,suffix,dryrun=False):
    for b in bands:
        bandname='%02d%02d'%(b[0]*10,b[1]*10)
        # Add FI and BI images
        # Last file is output name
        '''
        prefiles=[  # ACISBG
            ['15186/processed/%s_236_%s.img'%(prefix,bandname),
            '15186/processed/%s_5_%s.img'%(prefix,bandname),
            '15186/processed/%s_7_%s.img'%(prefix,bandname),
                '15186/processed/%s_%s.img'%(prefix,bandname)],
            ['16564/processed/%s_236_%s.img'%(prefix,bandname),
            '16564/processed/%s_5_%s.img'%(prefix,bandname),
            '16564/processed/%s_7_%s.img'%(prefix,bandname),
                '16564/processed/%s_%s.img'%(prefix,bandname)],
            ['16565/processed/%s_236_%s.img'%(prefix,bandname),
            '16565/processed/%s_5_%s.img'%(prefix,bandname),
            '16565/processed/%s_7_%s.img'%(prefix,bandname),
                '16565/processed/%s_%s.img'%(prefix,bandname)]
        ]
        '''
        prefiles=[ # READOUT BG / EVT3
            ['15186/processed/%s_236_%s.img'%(prefix,bandname),
            '15186/processed/%s_57_%s.img'%(prefix,bandname),
                '15186/processed/%s_%s.img'%(prefix,bandname)],
            ['16564/processed/%s_236_%s.img'%(prefix,bandname),
            '16564/processed/%s_57_%s.img'%(prefix,bandname),
                '16564/processed/%s_%s.img'%(prefix,bandname)],
            ['16565/processed/%s_236_%s.img'%(prefix,bandname),
            '16565/processed/%s_57_%s.img'%(prefix,bandname),
                '16565/processed/%s_%s.img'%(prefix,bandname)]
        ]
        for prefi in prefiles:
            cmd=['addimages']
            cmd.extend(prefi)
            print (' '.join(cmd))
            if not dryrun:
                subprocess.call(cmd)
        # Add projected images using reproject_image
        refimg='expmap/reproject_img_target.img'
        inobs=[15186,5005,16564,16565]
        infiles=','.join(['%s/processed/%s_%s.img'%(str(s),prefix,bandname) for s in inobs])
        cmd=['reproject_image', infiles, 'matchfile=expmap/reproject_img_target.img',
            'outfile=countimg/%s.%s'%(bandname,suffix), 'resolution=1', 'method=sum', 'verbose=3', 'clob+']
        print (' '.join(cmd))
        if not dryrun:
            subprocess.call(cmd)


def rescale_ro_img(bands,prefix,suffix,rescale):
    for b in bands:
        bandname='%02d%02d'%(b[0]*10,b[1]*10)
        cmd=['imarith','%s%s.%s'%(prefix,bandname,suffix),'=','%s%s.%s'%(prefix,bandname,suffix),'*',rescale]
        cmd=[str(s) for s in cmd]
        print (' '.join(cmd))
        subprocess.call(cmd)




def rescale_bg_img(evt3file,acisbgfile,bgimgs,ccd=None):
    '''
    Rescale ACIS background counts images to match 9.5-12.0 keV counts with
    corresponding event file. Use this for creating background images. Uses
    calc_bgimg_rescale().

    Parameters:

    evt3file - See calc_bgimg_rescale().
    acisbgfile - See calc_bgimg_rescale().
    bgimgs - Array of images paths to apply the same scaling to.
    ccd=None - See calc_bgimg_rescale().
    '''
    from .acisbg import calc_hardcount_ratio
    ratio = calc_bgimg_rescale(evt3file,acisbgfile,ccd)
    for bgimg in bgimgs:
        cmd=['imarith','jnkkkkkbgimg','=',bgimg,'*','%.8f'%ratio]
        #cmd=['dmimgcalc',bgimg,'infile2=','op=imgout=img1*%.8f'%ratio,'out=jnkkkkkbgimg','clob+']
        print (' '.join(cmd))
        subprocess.call(cmd)
        cmd=['mv','jnkkkkkbgimg',bgimg]
        print (' '.join(cmd))
        subprocess.call(cmd)


def cmd_make_count_image(evtfile, outfile, chips=None, energy=None, region=None, skybin=1):
    """
    >>> cmd_make_count_image('evt3.fits', '0540.img', chips=[2,3,6], energy=[500, 4000], region='fluximage.reg', skybin=1)
    "dmcopy evt3.fits'[ccd_id=2&&ccd_id=3&&ccd_id=6&&energy>500&&energy<4000&&sky=region(fluximage.reg)][bin sky=1]' \\!0540.img"
    """
    chipfilter = ""
    if chips is not None:
        chipfilter = '&&'.join(['ccd_id=%d'%_ for _ in chips]) + '&&'
    energyfilter = ""
    if energy is not None:
        energyfilter = "energy>{emin}&&energy<{emax}".format(emin=energy[0], emax=energy[1])
    regionfilter = ""
    if region is not None:
        regionfilter = '&&sky=region(%s)'%region
    imgbin = "bin sky=1"
    return "dmcopy {0}'[{1}{2}{3}][{4}]' \!{5}".format(
        evtfile, chipfilter, energyfilter, regionfilter, imgbin, outfile)


def fluximage(obsinfo, bands, bandnames, outputfile, skybin=1):
    """
    Make images using CHAV, combining all obsids.

    Create images for each observation first, then make a big image from them.

    Note that CHAV's chipmap2 needs a text file for bad pixels. The CIAO bad
    pixel file in FITS format has these regions, and a text format file can be
    generated using bpix_fits_to_txt.py. The default behaviour is the copy
    only bad pixel regions that persist through the entire observation, not
    those entries for pixels associated with afterglow with a duration of
    (typically) < 100 s. (See that program for details.)

    Arguments:
        obsinfo {obj} -- object created by .genie.loadInfoJSON
        bands {[[float, float]]} -- list of energy bands [emin, emax] in eV
        bandnames {[string]} -- list of suffixes for the bands
        outputfile {string} -- name of the output shell script file
        skybin {int} -- image binning (default=1)
    """
    from .genie import loadInfoJSON
    from .utils import run_cmd
    import os

    def make_images_obsid(obs, outname, emin, emax, spec, fluxreg, skybin=1):
        """
        Make files necessary for creating flux images for a single
        observation.

        Create counts images from the science event file, readout background
        event file, and ACIS background event file; create exposure map
        images. The images are cropped to the specified region file. The
        readout and ACIS background images are rescaled to their appropriate
        values so that images from different observations are ready to be
        co-added. Therefore, counts images are integer arrays while the others
        are double precision.

        To ensure that images from different observations are binned in the
        same way, use the same crop region file for all of them. This probably
        results in images with a lot of zeros, inflating the file sizes, so it
        would be a good idea to compress these intermediate products after
        making the big images. If position accuracy can tolerate +/- pixel
        (e.g. only binned final product is used) then things can be sped up by
        using the crop region specific to each observation.

        The lists of files for each type of product are returned as a tuple.

        Arguments:
            obs {object} -- a dictionary containing observation info
            emin {float} -- energy lower bound (eV)
            emax {float} -- energy upper bound (eV)
            fluxreg {string} -- path to crop region file
        """
        import os
        from .utils import ccdid_to_chipname

        if not os.path.isdir(obs['folder']+'/'+'images'):
            os.mkdir(obs['folder']+'/'+'images')

        prefix = 'images/' + outname

        imgs = []; ros = []; bgs = []; exps = []
        output = ""

        # Make images for event3 and readout files
        for evt3 in obs['evtfiles']:
            suffix = ''.join(map(str, evt3['chips']))
            outfile = prefix + '.img' + suffix
            outfile_ro = prefix + '.ro' + suffix
            outfile_exp = prefix + '.exp' + suffix
            output += image_from_events(obs['folder']+'/'+evt3['file'],
                              obs['folder']+'/'+outfile,
                              chips=evt3['chips'],
                              energy=[emin, emax],
                              region=fluxreg,
                              skybin=skybin)
            output += "\n"
            imgs.append(obs['folder']+'/'+outfile)
            output += image_from_events(obs['folder']+'/'+evt3['readoutfile'],
                              obs['folder']+'/'+outfile_ro,
                              chips=evt3['chips'],
                              energy=[emin, emax],
                              region=fluxreg,
                              skybin=skybin)
            output += "\n"
            output += image_rescale(obs['folder']+'/'+outfile_ro, evt3['rorescale'])
            output += "\n"
            ros.append(obs['folder']+'/'+outfile_ro)
            # Exposure maps
            cmd="""chipmap2 -emin {emi} -emax {ema} -evtfile {evtfile} -o jnk -bin {skybin} -chips {chipnames} -t {t} -abund {abund} -z {z} -nh {nh} -badpixfilter {usrbpix} @chipmap.par
convaspect -chipmap jnk -asphist {aspecthist} -o jnk1
projectimg from=jnk1 onto={ctimg} out={expfile} exact=yes subpix=1
rm -rf jnk jnk1
""".format(
                    t=spec['t'],
                    abund=spec['abund'],
                    z=spec['z'],
                    nh=spec['nh'],
                    emi=emin/1000.,
                    ema=emax/1000.,
                    evtfile=obs['folder']+'/'+evt3['file'],
                    skybin=skybin,
                    chipnames=','.join(ccdid_to_chipname(evt3['chips'])),
                    aspecthist=obs['folder']+'/'+evt3['aspectfile'],
                    ctimg=obs['folder']+'/'+outfile,
                    expfile=obs['folder']+'/'+outfile_exp,
                    usrbpix=obs['folder']+'/'+obs['bpixtxt']
                    )
            output += cmd
            output += "\n"
            exps.append(obs['folder']+'/'+outfile_exp)

        # Make images for acis background
        for bg in obs['bgfiles']:
            suffix = ''.join(map(str, bg['chips']))
            outfile = prefix + '.bg' + suffix
            output += image_from_events(obs['folder']+'/'+bg['file'],
                              obs['folder']+'/'+outfile,
                              chips=bg['chips'],
                              energy=[emin, emax],
                              region=fluxreg,
                              skybin=skybin)
            output += "\n"
            output += image_rescale(obs['folder']+'/'+outfile, bg['ctrescale'])
            output += "\n"
            bgs.append(obs['folder']+'/'+outfile)

        return imgs, ros, bgs, exps, output

    def make_big_images(bandname, imglist, rolist, bglist, explist):
        """
        Combine sets of image files and make a big flux image.

        Create co-added counts images and co-added exposure map images. Create a
        flux image by dividing the sum of co-added counts images by the co-added
        exposure map.

        Arguments:
            imglist {list(string)} -- paths to files
            rolist {list(string)} -- paths to files
            bglist {list(string)} -- paths to files
            explist {list(string)} -- paths to files
        """
        prefix = 'images/'+bandname
        output = ""
        output += 'addimages '+' '.join(imglist)+' '+prefix+'.img' + "\n"
        output += 'addimages '+' '.join(rolist)+' '+prefix+'.ro' + "\n"
        output += 'addimages '+' '.join(bglist)+' '+prefix+'.bg' + "\n"
        output += 'addimages '+' '.join(explist)+' '+prefix+'.exp' + "\n"
        output += "imarith jnk = {0}.img - {0}.ro".format(prefix) + "\n"
        output += "imarith jnk = jnk - {0}.bg".format(prefix) + "\n"
        output += "imarith {0}.flux = jnk / {0}.exp".format(prefix) + "\n"
        output += "rm -rf jnk" + "\n\n"
        return output


    def image_from_events(evtfile, outfile, chips=None, energy=None, region=None, skybin=1):
        """
        >>> image_from_events('evt3.fits', '0540.img', chips=[2,3,6], energy=[500, 4000], region='fluximage.reg', skybin=1)
        "dmcopy evt3.fits'[ccd_id=2&&ccd_id=3&&ccd_id=6&&energy>500&&energy<4000&&sky=region(fluximage.reg)][bin sky=1]' \\!0540.img"
        """
        #from .wrap import ciao_dmcopy
        dmfilter = []
        if chips is not None:
            dmfilter.append( 'ccd_id=' + ','.join(str(_) for _ in chips) )
        if energy is not None:
            dmfilter.append( 'energy>{0}&&energy<{1}'.format(energy[0], energy[1]) )
        if region is not None:
            dmfilter.append( 'sky=region(%s)'%region )
        if dmfilter == []:
            dmfilter = ''
        else:
            dmfilter = '[' + '&&'.join(dmfilter) + ']'
        imgbin = "bin sky=%d"%skybin
        cmd = ["{0}'{1}[{2}]'".format(evtfile, dmfilter, imgbin),
               "\!{0}".format(outfile)]
        #ciao_dmcopy(cmd)
        return "dmcopy "+' '.join(cmd)

    def image_rescale(imgfile, rescale):
        return "imarith {0} = {0} '*' {1}".format(imgfile, rescale)



    if not os.path.isdir('images'):
        os.mkdir('images')

    if type(obsinfo) == type(''):
        obsinfo = loadInfoJSON(obsinfo)

    fluxreg = obsinfo['reg_crop']
    spec = {'t': obsinfo['t'],
            'z': obsinfo['z'],
            'abund': obsinfo['abund'],
            'nh': obsinfo['nh']}

    output = "#!/bin/bash\n"

    for (emin, emax), bandname in zip(bands, bandnames):
        imglist = []
        rolist = []
        bglist = []
        explist = []
        for obs in obsinfo['obs']:
            imgs, ros, bgs, exps, cmds = make_images_obsid(obs, bandname, emin, emax, spec, fluxreg, skybin)  # Get list of product files
            imglist.extend(imgs)
            rolist.extend(ros)
            bglist.extend(bgs)
            explist.extend(exps)
            output += cmds

        output += make_big_images(bandname, imglist, rolist, bglist, explist)

    with open(outputfile, 'w') as fh:
        fh.write(output)

    run_cmd("chmod +x %s"%outputfile, shell=True, quiet=True)

    return True
