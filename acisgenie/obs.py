"""
An Obs class for observations.
"""


class Obs(object):

    verbose = True

    def __init__(self, obsid):
        import os
        if os.path.isdir(obsid):
            # Does it have primary/obsid_evt2.fits?
            if os.path.isfile('%d/primary/%d_evt2.fits' % obsid):
                # Chips? Pointing? Period?
                pass
        else:
            if not os.path.exists(obsid):
                pass
                # Create and download stuff?
            else:
                self.error('Path %s is not a valid directory' % obsid)

    def error(self, msg):
        if self.verbose:
            print ('Error: %s!' % msg)
        else:
            print ('Error')
