"""
Configuration and checks.
"""
import os
from . import conf


CALDB = None
ACISBG = None
ACISGENIE_DYLIB_PATH = None
PASSCHECK = False

MSG_BLOCKED = 'Stopping: acisgenie was not properly configured.'


def block(verbose=True):
    """
    Return True if missing configuration is likely to prevent proper
    execution.
    """
    if not conf.PASSCHECK:
        if verbose:
            print(MSG_BLOCKED)
        return True
    else:
        return False


def check_caldb():
    """
    Check whether CALDB is properly configured.
    """
    caldb = os.environ.get('CALDB')
    if not (caldb is None):
        if os.path.exists('%s/data/chandra/acis/caldb.indx' % caldb):
            conf.CALDB = caldb
            return True

    return False


def check_acisbg():
    """
    Check whether user ACISBG folder exists.
    """
    acisbg = os.environ.get('ACISBG')
    if not (acisbg is None):
        if os.path.exists('%s' % acisbg):
            conf.ACISBG = acisbg
            return True

    return False
