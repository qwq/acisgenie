"""
Wrappers for some command line tools.
"""
from .utils import run_cmd
import time


def ftools_addarf(outname, filelist, weights, debugmode=False):
    """
    Wrapper to call addarf to add ARF files using weights. Product will be
    named outname.

    Requires:

    addarf -- HEASOFT
    """
    import numpy as np
    np.random.seed(int(time.time()))
    affix = np.random.randint(0, 10000)
    content = []
    with open("tmp%s_arflist" % affix, 'w') as f:  # input file for addspec
        for i in np.arange(len(filelist)):
            content.append("%s %f" % (filelist[i], weights[i]))
        f.write('\n'.join(content))
    cmd = ['addarf', "@tmp%s_arflist" % affix, 'out_ARF=!%s' % outname]
    try:
        log = str(run_cmd(cmd, output=True))
    except subprocess.CalledProcessError:
        print ("Error: addarf raised an error. Check log file.")
        return False
    else:
        with open("addarf_%s.log" % outname, 'w') as f:
            f.write(' '.join(cmd)+'\n')
            f.write('\n'.join(content)+'\n')
            f.write(log)
        if not debugmode:
            run_cmd(['rm', '-rf', "tmp%s_arflist" % affix])  # remove temp list file
        return True


def ftools_addrmf(outname, filelist, weights, debugmode=False):
    """
    Wrapper to call addrmf to add RMF files using weights. Product will be named outname.

    Requires:

    addrmf -- HEASOFT
    """
    import numpy as np
    np.random.seed(int(time.time()))
    affix = np.random.randint(0, 10000)
    content = []
    with open("tmp%s_rmflist" % affix, 'w') as f:  # input file for addspec
        for i in np.arange(len(filelist)):
            content.append("%s %f" % (filelist[i], weights[i]))
        f.write('\n'.join(content))
    cmd = ['addrmf', "@tmp%s_rmflist" % affix, 'rmffile=!%s' % outname]
    try:
        log = str(run_cmd(cmd, output=True))
    except subprocess.CalledProcessError:
        print ("Error: addrmf raised an error. Check log file.")
        return False
    else:
        with open("addrmf_%s.log" % outname, 'w') as f:
            f.write(' '.join(cmd)+'\n')
            f.write('\n'.join(content)+'\n')
            f.write(log)
        if not debugmode:
            run_cmd(['rm', '-rf', "tmp%s_rmflist" % affix])  # remove temp list file
        return True


def ftools_addspec(outname, filelist):
    """
    Add PHA files by coadding. Product will be named outname.

    Note: does not use any advanced addspec options, only does straightforward
    coadding. No longer use this in spectrum extraction script
    extract_spectrum(), replaced by add_spectra(), which uses only Python
    interface and will also take care of the related readout and ACIS
    background files.

    Requires:

    addspec -- HEASOFT
    """
    import numpy as np
    np.random.seed(int(time.time()))
    affix = np.random.randint(0, 10000)
    with open("tmp%s_phalist_%s" % (affix, outname), 'w') as f:  # input file for addspec
        f.write('\n'.join(filelist))
    cmd = ['addspec', "tmp%d_phalist_%s" % (affix, outname), '!tmp%d_product_%s' % (affix, outname),
           'qaddrmf=no', 'qsubback=no']
    log = ''
    try:
        log = str(run_cmd(cmd, output=True))
    except subprocess.CalledProcessError:
        print ("Error: addspec raised an error. Check log file.")
        returnval = False
    else:
        run_cmd(['mv', 'tmp%d_product_%s.pha' % (affix, outname), outname])
        run_cmd(['rm', '-rf', "tmp%s_phalist_%s" % (affix, outname)])  # remove temp list file
        returnval = True
    with open("addspec_%s.log" % outname, 'w') as f:
        f.write(' '.join(cmd)+'\n')
        f.write('\n'.join(filelist)+'\n')
        f.write(log)
    return returnval


def ciao_dmlist(args):
    from .utils import run_cmd
    cmd = ['dmlist']
    cmd.extend(args)
    return run_cmd(cmd, output=True)


def ciao_dmkeypar(filepath, keyword):
    from .utils import run_cmd
    cmd = ['dmkeypar']
    cmd.extend([filepath, keyword, 'echo=yes'])
    return run_cmd(cmd, output=True)


def ciao_dmcopy(args):
    from .utils import run_cmd
    cmd = ['dmcopy']
    cmd.extend(args)
    return run_cmd(cmd, output=True)


def ciao_dmhedit(infile, operation, key, value, filelist="none", datatype="indef",
                 unit=None, comment=None, verbose=0):
    from .utils import run_cmd
    cmd = ['dmhedit']
    cmd.extend([infile, filelist, operation, "key=%s" % key, "value=%s" % value, "datatype=%s" % datatype])
    if unit is not None:
        cmd.extend(["unit='%s'" % unit])
    if comment is not None:
        cmd.extend(["comment='%s'" % comment])
    cmd.extend(["verbose=%d" % verbose])
    return run_cmd(cmd, output=True)


def ciao_dmmerge():
    # TODO
    pass


def chav_badpixfilter():
    # TODO
    pass


def chav_runextrspec(outname, regfile, evtfile, gtifile, aspfile, parfile,
                     bg_file=None, readout_bg_file=None, chips=None, exclregfile=None,
                     together=True, nochipname=True, dormf=True):
    """
    Wrapper that calls runextrspec to extract a spectrum and associated files.

    Inputs:

    outname -- (string) base of the output files
    parfile -- (string) parameter file for runextrspec, e.g. 'spec.par'
    regfile -- (string) extraction region file, e.g. 'cluster.reg'
    evtfile -- (string) event file, e.g. 'evt3.fits'
    gtifile -- (string) GTI file, e.g. 'clean.gti'
    aspfile -- (string) aspect offset file, e.g. 'aoff1.fits'
    bg_file -- (string) acis background event file, e.g. 'acisbg.fits'
    readout_bg_file -- (string) readout background file, e.g. 'readoutbg.fits'
    chips -- ([int]) list of chips, e.g. [0, 1, 2, 3, 6]
    exclregfile -- (string) file containing (positive) regions that will be excluded
    together -- if False, chips will be processed separately and output files appended
    with _reg*

    Output:

    True/False for successful execution.

    Files produced: *.pha from main event file, *.bg from background event
    file, *.rdt from readout event file, *.arf and *.rmf

    Requires:

    runextrspec -- CHAV
    """
    from os.path import isfile
    import astropy.io.fits as pf
    from .utils import run_cmd, ccdid_to_chipname
    import subprocess

    MSG_E_NOACISBGFILE = """*** Error ***

ACIS background file specified, but it is not found.
"""
    MSG_E_NOACISBGGTI = """*** Error ***

ACIS background file specified, but it does not have a GTI extension. A valid
GTI extension is required by runextrspec. You may need to append the GTI
extension of the blank sky dataset to it.
"""
    MSG_E_NOREADOUTBGFILE = """*** Error ***

Readout background file specified, but it is not found.
"""
    MSG_E_NOREADOUTBGGTI = """*** Error ***

Readout background file specified, but it does not have a GTI extension. A
valid GTI extension is required by runextrspec. You may need to use
readout_append_gti to append a GTI extension with the appropriate exposure
time.
"""
    MSG_E_NOREGFILE = """*** Error ***

Region file not found. A spectral extraction region file is required.
"""
    MSG_E_NOEXCLREGFILE = """*** Error ***

An exclusion region file is specified, but it is not found.
"""
    MSG_E_NOINPUTFILE = """*** Error ***

{file} not found. {file} is required.
"""
    halt = False

    # Check required input files exist, because runextrspec doesn't, and
    # missing file errors pop up after partial execution
    inputlist = {'Event file': evtfile, 'Clean GTI file': gtifile,
                 'Aspect file': aspfile, 'Param file': parfile}
    for k in inputlist:
        if not isfile(inputlist[k]):
            print (MSG_E_NOINPUTFILE.format(file=k))
            halt = True

    # Check that readout file has GTI extension
    if bg_file is not None:
        if isfile(bg_file):
            t = pf.open(bg_file)
            # Use HDUNAME if it exists, otherwise look for EXTNAME
            gti_count = len([1 for x in t
                             if 'GTI' in x.header[('EXTNAME', 'HDUNAME')['HDUNAME' in x.header]]])
            if gti_count < 1:
                print (MSG_E_NOACISBGGTI)
                halt = True
        else:
            print (MSG_E_NOACISBGFILE)
            halt = True

    # Check that acisbg file has GTI extension
    if readout_bg_file is not None:
        if isfile(readout_bg_file):
            t = pf.open(readout_bg_file)
            # Use HDUNAME if it exists, otherwise look for EXTNAME
            gti_count = len([1 for x in t
                             if 'GTI' in x.header[('EXTNAME', 'HDUNAME')['HDUNAME' in x.header]]])
            if gti_count < 1:
                print (MSG_E_NOREADOUTBGGTI)
                halt = True
        else:
            print (MSG_E_NOREADOUTBGFILE)
            halt = True

    # Check region files exist
    if not isfile(regfile):
        print (MSG_E_NOREGFILE)
        halt = True
    if exclregfile is not None and not isfile(exclregfile):
        print (MSG_E_NOEXCLREGFILE)
        halt = True

    if halt:
        return False

    cmd = ['runextrspec']
    cmd.extend(['-o', outname, '-regs', regfile, '-evtfile', evtfile,
                '-aspfile', aspfile, '-gtifile', gtifile])
    if chips is not None:
        cmd.extend(['-chips', ','.join(ccdid_to_chipname(chips))])
    else:
        cmd.extend(['-chips', 'all'])
    if exclregfile is not None:
        cmd.append('exclreg=%s' % exclregfile)
    if together:
        cmd.append('-together')
    if nochipname:
        cmd.append('-nochipname')
    if bg_file is not None:
        cmd.extend(['-dobg', '-bg_file', bg_file])
    if readout_bg_file is not None:
        cmd.extend(['-doreadout', '-readout_bg_file', readout_bg_file])
    if dormf:
        cmd.append('-dormf')
    cmd.append("@%s" % parfile)
    try:
        output = run_cmd(cmd, output=True)
    except subprocess.CalledProcessError:
        return False
    else:
        # TODO write to log file
        print ("Done running runextrspec, you need to correct the BACKSCAL keyword")
        return True


def make_acisbg():
    # TODO
    pass


def zhtools_gtiexp(fitsfile, ext=2):
    """
    Get good time interval from a GTI extension using gtiexp.

    Requires:

    gtiexp -- ZHTOOLS
    """
    import re
    cmd = ['gtiexp', "%s+%d" % (fitsfile, ext)]
    import subprocess
    try:
        output = run_cmd(cmd, output=True, quiet=True)
    except subprocess.CalledProcessError:
        return False
    else:
        rs = re.findall(r'\d+.\d+', output)
        if len(rs) == 1:
            exp = float(rs[0])
            return exp
        else:
            print ("Error: expecting exactly one float from gtiexp, got %d" % len(rs))
            return False
