"""
An EventFile class for an event file.
"""
import os
import astropy.io.fits as pf
from .fits import FitsFile


class EventFile(FitsFile):

    def __init__(self, path):
        super(EventFile, self).__init__(path)
        evtfile_properties = {
            'pointing': self.get_pointing,
            'chips': self.get_chips,
            'frametime': self.get_frametime,
            'nominal_exposure': self.get_nominal_exposure,
            'acisbgperiod': self.get_acisbg_period,
            'acisbg': self.get_acisbg_file,
            'events_header': self.get_events_header
        }  # For FitsFile.__getattr__
        self.properties.update(evtfile_properties)

    def get_events_header(self):
        return self.get_header('EVENTS')

    def get_pointing(self):
        """
        Determines whether pointing is ACIS-I or ACIS-S, returning 'i' or 's'
        correspondingly. This uses the fact that the listed nominal aimpoint
        C{SIM_Z} is different:

            - ACIS-I: -233.587
            - ACIS-S: -190.143

        The value contained in the header may deviate slightly, so the closer of
        the two is picked.

        Note: intended to distinguish between ACIS-I and ACIS-S. If some other
        nominal aimpoint is intended, this will not make sense.

        @returns: (string) 'i' for ACIS-I or 's' for ACIS-S pointing.
        """
        try:
            sim_z = self.events_header.raw['SIM_Z']
        except IndexError:
            self.error('SIM_Z param not found in header')
            return False
        else:
            diff = [abs(-233.587 - sim_z), abs(-190.143 - sim_z)]
            # returns choice w/ smaller diff
            return ['i', 's'][diff[1] < diff[0]]

    def get_chips(self):
        """
        Returns a list of C{ccd_id} contained in the C{DETNAM} keyword. Note that
        this keyword may have been modified from archival value by data processing
        steps.

        @returns: [int] containing CCD ids.
        """
        try:
            chips = [int(s) for s in list(
                self.events_header.raw['DETNAM']) if s.isdigit()]
            return chips
        except IndexError:
            self.error("DETNAM param not found in EVENTS header")
            return None

    def get_nominal_exposure(self):
        """
        Returns the nominal exposure of the observation. This is contained in the
        EXPTIME keyword. Typically it is 3.1 s for 5-CCD exposure and 3.2 s for 6-CCD.
        """
        return self.events_header.raw['EXPTIME']

    def get_frametime(self):
        """
        Get the frame time from an EVENTS extension header. Frame time is nominal
        exposure + read-out time.
        """
        return self.events_header.raw['TIMEDEL']

    def get_exposure(self):
        """
        Return the total exposure time calculated from the GTI extension. If there are many, default to the first found.
        """
        if 'GTI' in self.fits:
            gti = GtiExtension(self.fits['GTI'])
            return gti.gti_exp()
        else:
            return None

    def get_readout_eff_exp(self):
        """
        Return the effective exposure time of the simulated readout events file. Readout time is 0.04104 s.
        """
        gtiexp = self.get_exposure()
        return gtiexp * self.frametime / 0.04104

    def get_readout_rescale(self):
        """Return upscaling factor for readout effective exposure."""
        return self.frametime / 0.04104

    def get_start_time(self):
        """Get observation start time."""
        return self.events_header.get_key('TSTART')

    def get_obs_date(self):
        """Get datetime object of the date of observation from the header."""
        import datetime
        return datetime.datetime.strptime(self.events_header.get_key('DATE-OBS')[:10], "%Y-%m-%d")

    def get_acisbg_period(self, verbose=False):
        """Look up applicable ACIS background period using acisbg_period() with obsdate in header.
        """
        from .acisbg import acisbg_period
        obsdate = self.get_obs_date()
        if verbose:
            print("Observation date: ", obsdate)
        return acisbg_period(obsdate, verbose=verbose)

    def get_acisbg_file(self, verbose=False):
        """
        Looks up list of ACIS background event files corresponding to each
        chip and add them to self.acisbg.
        """
        from .acisbg import lookup_acisbg_file
        return lookup_acisbg_file(self.acisbgperiod, self.chips, self.pointing, verbose=verbose)

    def check_acisbg_file(self):
        """
        Check ACIS background files exist, and have the same GAINFILE applied.
        """
        evt3gainfile = self.events_header.raw['GAINFILE']
        for bgfile in self.acisbg:
            # fp='%s/data/chandra/acis/bkgrnd/%s'%(chandra.CALDB, bgfile) # CALDB directory
            fp = '%s/data/%s' % (chandra.ACISBG, bgfile)  # User directory
            if not os.path.isfile(fp):
                self.error('File %s does not exist!' % fp)
            else:
                t = pf.open(fp)
                bggainfile = t['EVENTS'].header['GAINFILE']
                t.close()
                if bggainfile != evt3gainfile:
                    self.error('The background %s has GAINFILE\n%s\nbut event file was processed using\n%s' % (
                        bgfile, bggainfile, evt3gainfile))


class GtiExtension(object):

    def __init__(self, ext):
        self.gti = ext

    def gti_exp(self):
        """
        Sum all entries of a GTI FITS table.

        @param gti: GTI extension
        @type gti: HDU.data, e.g. C{evtfile['GTI'].data}
        @returns: (float) sum of the good time intervals in the GTI extension
        """
        # Check GTI table column defs has START and STOP
        if not ("name = 'start'" in repr(self.gti.data.columns.columns).lower() and
                "name = 'stop'" in repr(self.gti.data.columns.columns).lower()):
            print("Error: GTI extension must have START and STOP columns")
            return False
        return sum(self.gti.data.field(1)) - sum(self.gti.data.field(0))


def append_gti(targetfile, tstart_arr, tstop_arr, strip_gti=False):
    """
    Function: append_gti

    Append a GTI extension to the target file and write the TSTART and TSTOP
    columns with values in inputs tstart_arr and tstop_arr.

    If strip_gti=True, existing GTI extensions are removed first.

    Returns True if successful, False if specified file is not found.
    """
    try:
        tf = pf.open(targetfile)
    except FileNotFoundError:
        print('Error: file %s not found!' % targetfile)
        return False

    if strip_gti:
        for i in reversed(range(1, len(tf))):  # Skip primary HDU
            if tf[i].header['EXTNAME'][:3] == 'GTI':
                del tf[i]

    bthdu = create_gti(tstart_arr, tstop_arr)

    tf.append(bthdu)
    tf.writeto(targetfile, overwrite=True)
    return True


def create_gti(tstart_arr, tstop_arr):
    """
    Create a GTI extension using two arrays (TSTART and TSTOP).

    Return:
    BinTableHDU object.
    """
    if len(tstart_arr) != len(tstop_arr):
        print("TSTART and TSTOP arrays must have the same length!")
        return False
    if len(tstart_arr) == 0:
        print("TSTART has zero length!")
        return False

    bthdu = pf.BinTableHDU.from_columns([
        pf.Column(name='start', format='1D', array=tstart_arr),
        pf.Column(name='stop', format='1D', array=tstop_arr)
    ])
    bthdu.header['EXTNAME'] = 'GTI'
    bthdu.header['HDUNAME'] = 'GTI'
    bthdu.header['TUNIT1'] = 's'
    bthdu.header['TUNIT2'] = 's'
    bthdu.header['MISSION'] = 'AXAF'
    bthdu.header['TELESCOP'] = 'CHANDRA'
    bthdu.header['INSTRUME'] = 'ACIS'
    bthdu.header['TIMESYS'] = 'TT'
    bthdu.header['TIMEUNIT'] = 's'

    return bthdu


def fake_gti(exposure):
    """
    Create a fake GTI extension with a single row to effect the specified
    exposure time.
    """
    assert (isinstance(exposure, int) or isinstance(
        exposure, float)), 'Exposure must be an int or float.'
    gtiext = create_gti([0.], [exposure])
    gtiext.header['EXPOSURE'] = (exposure, 'Fake GTI exposure')
    gtiext.header['COMMENT'] = 'Fake GTI extension for the exposure time.'
    return gtiext
