"""
Functions dealing with the ACIS background.

The main tool here is create_from_evtfile(), which uses information from an
event file and aspect solution file to create an appropriate (set of)
ACIS blank sky background event files, using the CALDB data set.
The reprojection is performed by make_acisbg (an external compiled program).

"""


def create_from_evtfile(
    evtfile, asolfile,
    gtifile=None, period=None, outname='acisbg_{ccds}',
    badpixfile=None):
    """
    Optional:

    gtifile - Apply a time filter to the asolfile.
    period - Override the default period that is based on observation date.
    outname - By default this is 'acisbg_{ccds}', where {ccds} will be replaced by a concatenation of the CCD IDs contained.
    badpixfile - A text file for CHAV's badpixfilter to apply to the resulting files.
    """
    # TODO
    pass


def acisbg_period(obsdate, verbose=False):
    """
    Find the ACIS background epoch (period A-G) from observation date. The
    epochs end on:

        - A. 1999-09-16
        - B. 2000-01-28
        - C. 2000-11-30
        - D. 2005-08-31
        - E. 2009-09-20
        - F. 2011-12-31

    @param obsdate: observation date.
    @type obsdate: datetime
    @param verbose: whether to print the epoch.
    @type verbose: bool
    @returns: (string) 'A', 'B', 'C', 'D', 'E', or 'F' if the date falls into
    one of their ranges, or 'G' if the date is after the end of period F.
    """
    import datetime
    periodA = datetime.datetime.strptime(
        '1999-09-16', "%Y-%m-%d")  # End of period A
    periodB = datetime.datetime.strptime(
        '2000-01-28', "%Y-%m-%d")  # End of period B
    periodC = datetime.datetime.strptime(
        '2000-11-30', "%Y-%m-%d")  # End of period C
    periodD = datetime.datetime.strptime(
        '2005-08-31', "%Y-%m-%d")  # End of period D
    periodE = datetime.datetime.strptime(
        '2009-09-20', "%Y-%m-%d")  # End of period E
    periodF = datetime.datetime.strptime(
        '2011-12-31', "%Y-%m-%d")  # End of period F
    if obsdate <= periodA:
        p = 'A'
    elif obsdate <= periodB:
        p = 'B'
    elif obsdate <= periodC:
        p = 'C'
    elif obsdate <= periodD:
        p = 'D'
    elif obsdate <= periodE:
        p = 'E'
    elif obsdate <= periodF:
        p = 'F'
    else:
        p = 'G'
    if verbose:
        print("ACIS background period %s" % p)
    return p


def lookup_caldb_file(period, chips, pointing, verbose=False):
    """
    Look up file name for ACIS background by period, pointing and chip number,
    from the CALDB files. The main practical difference from just using
    acis_bkgrnd_lookup on an event file, is the ability to specify the period.
    """
    # TODO
    pass


def lookup_acisbg_file(period, chips, pointing, verbose=False):
    """
    Look up filename for ACIS background by period, pointing and chip number.
    For an array of input ccd_id, returns an array of string corresponding to
    the correct ACIS background files.

    Note: must check C{GAINFILE} keyword -- if the background file was
    processed using an older version, it needs to be reprocessed. The
    chandra.eventfile.EventFile object does check this.

    @param period: ACIS background epoch e.g. 'E'
    @type period: string
    @param chips: list of chip numbers
    @type chips: [int]
    @param pointing: observation pointing, 'i' or 's'
    @type pointing: string
    @param verbose: whether to print additional messages
    @type verbose: boolean
    @returns:
        - If an appropriate ACIS background file is found for ALL chips,
          return an array of file paths to those files.
        - If only some are found, return False and print a message listing the
          chips for which no background is found.
        - If no background is found, return False and print a message.
    """
    if pointing != 'i' and pointing != 's':
        print("Error: expecting pointing = 'i' or 's', got %s" % pointing)
        return False

    # Use period E files for period F ACIS-I because of longer exposure;
    # ACIS-S in period F is longer so use them.
    if period == 'F' and pointing == 'i':
        print("Notice: period E background is used for period F ACIS-I pointings.")

    # Look up table. Some existing files in acisbg/data are not listed because
    # I have yet to use them so have not checked their GAINFILE.

    # Update 20170330: all available files listed below; some of the older
    # ones may not have the newest GAINFILE but should be OK (updates affect
    # newer observations).

    # Update 20190515: added period G files. These use GAINFILE
    # acisD2000-01-29gain_ctiN0007.fits. Did not update previous files.
    bgfiles = {
        'A': [
            {'aim': 's', 'chip': '6', 'file': 'aciss_A_6_bg_evt_041104.fits'},  # 68903 s
            {'aim': 's', 'chip': '7', 'file': 'aciss_A_7_bg_evt_041104.fits'}  # 73609 s
        ],
        'B': [
            {'aim': 'i', 'chip': '01236', 'file': 'acisi_B_01236_bg_evt_201215.fits'},  # 250000 s
            {'aim': 'i', 'chip': '7',     'file': 'acisi_B_7_bg_evt_201215.fits'},  # 55000 s
            {'aim': 's', 'chip': '2368', 'file': 'aciss_B_2368_bg_evt_041104.fits'},  # 165000 s
            {'aim': 's', 'chip': '5', 'file': 'aciss_B_5_bg_evt_041104.fits'},  # 55000 s
            {'aim': 's', 'chip': '7', 'file': 'aciss_B_7_bg_evt_041104.fits'},  # 145000 s
            {'aim': 's', 'chip': '9', 'file': 'aciss_B_9_bg_evt_041104.fits'}  # 50000 s
        ],
        'C': [
            {'aim': 'i', 'chip': '01236', 'file': 'acisi_C_01236_bg_evt_070707.fits'},  # 450000 s
            {'aim': 'i', 'chip': '7',     'file': 'acisi_C_7_bg_evt_070707.fits'},  # 100000 s
            {'aim': 's', 'chip': '236',   'file': 'aciss_C_236_bg_evt_070707.fits'},  # 180000 s
            {'aim': 's', 'chip': '5',     'file': 'aciss_C_57_bg_evt_070707.fits'},  # 54000 s
            {'aim': 's', 'chip': '7',     'file': 'aciss_C_7_bg_evt_070707.fits'},  # 110000 s
            {'aim': 's', 'chip': '8',     'file': 'aciss_C_8_bg_evt_070707.fits'},  # 140000 s
            {'aim': 's', 'chip': '1',     'file': 'aciss_C_1_bg_evt_070707.fits'}  # 86000 s
        ],
        'D': [
            {'aim': 'i', 'chip': '01236', 'file': 'acisi_D_01236_bg_evt_300407.fits'},  # 1500000 s
            {'aim': 'i', 'chip': '7',     'file': 'acisi_D_7_bg_evt_300407.fits'},  # 340000 s
            {'aim': 'i', 'chip': '8',     'file': 'acisi_D_8_bg_evt_300407.fits'},  # 88000 s
            {'aim': 's', 'chip': '236',   'file': 'aciss_D_236_bg_evt_030507.fits'},  # 500000 s
            {'aim': 's', 'chip': '5',     'file': 'aciss_D_57_bg_evt_030507.fits'},  # 170000 s
            {'aim': 's', 'chip': '7',     'file': 'aciss_D_7_bg_evt_030507.fits'},  # 450000 s
            {'aim': 's', 'chip': '8',     'file': 'aciss_D_8_bg_evt_030507.fits'},  # 100000 s
            {'aim': 's', 'chip': '1',     'file': 'aciss_D_1_bg_evt_030507.fits'}  # 220000 s
        ],
        'E': [
            {'aim': 'i', 'chip': '01236', 'file': 'acisi_E_01236_bg_evt_201209.fits'},  # 1550000 s
            {'aim': 'i', 'chip': '7',     'file': 'acisi_E_7_bg_evt_201209.fits'},  # 310000 s
            {'aim': 's', 'chip': '23567', 'file': 'aciss_E_23567_bg_evt_110210.fits'},  # 450000 s
            {'aim': 's', 'chip': '8',     'file': 'aciss_E_8_bg_evt_110210.fits'}  # 160000 s
        ],
        'F': [
            {'aim': 'i', 'chip': '01236', 'file': 'acisi_E_01236_bg_evt_201209.fits'},  # 1550000 s
            {'aim': 'i', 'chip': '7',     'file': 'acisi_E_7_bg_evt_201209.fits'},  # 310000 s
            {'aim': 's', 'chip': '23567', 'file': 'aciss_F_23567_bg_evt_010715.fits'},  # 800000 s
            {'aim': 's', 'chip': '8',     'file': 'aciss_F_8_bg_evt_010715.fits'}  # 380000 s
        ],
        'G': [
            {'aim': 'i', 'chip': '0123',  'file': 'acisi_G_0123_bg_evt_150519.fits'},  # 600000 s
            {'aim': 'i', 'chip': '6',     'file': 'acisi_G_6_bg_evt_150519.fits'},  # 300000 s
            {'aim': 's', 'chip': '25',    'file': 'aciss_G_25_bg_evt_150519.fits'},  # 600000 s
            {'aim': 's', 'chip': '3678',  'file': 'aciss_G_3678_bg_evt_150519.fits'}  # 700000 s
        ]
    }
    if period in bgfiles:
        flist = list(chips)  # List of bg files
        count = 0
        for entry in bgfiles[period]:
            if entry['aim'] is pointing:
                # Match chips in lookup to those required
                # a = [int(c) for c in list(entry['chip']) if int(c) in chips]
                for c in list(entry['chip']):
                    if int(c) in chips:
                        flist[flist.index(int(c))] = entry['file']
                        count += 1
        if count < len(chips):
            print("Error: background file not found for %d chips (ACIS-%s mode)" %
                  (len(chips) - count, pointing.upper()))
            print("These chips: %s" %
                  (','.join([str(c) for f, c in zip(flist, chips) if f == c])))
            return False
        else:
            # Successful look up
            if verbose:
                print("ACIS background files:")
                print(flist)
            return flist
    else:
        print("Error: period %s is not in the look up list." % period)
        return False


def calc_hardcount_ratio(evtfile, bgfile, ccd=None):
    """
    Calculate the ratio of hard (9.5-12 keV) counts in the science file vs.
    the background file. Before using this for creating background images, it
    should be reduced to account for the amount of ACIS background contained
    in the readout image (use calc_bgimg_rescale instead).

    @param evtfile: event file path
    @type evtfile: string
    @param bgfile: background event file path
    @type bgfile: string
    @param ccd: (optional) filter by C{ccd_id}
    @type ccd: [int]

    @return: (float) ratio of hard counts
    """
    from .utils import require_file
    from .fits import eventfile_energy_between
    if not require_file([evtfile, bgfile]):
        print("Error: could not calculate hardcount ratio (file not found)")
        return False
    sc = eventfile_energy_between(evtfile, 9500, 12000, ccd=ccd)  # evt3 counts
    ac = eventfile_energy_between(
        bgfile, 9500, 12000, ccd=ccd)  # acisbg counts
    return 1. * sc / ac


def calc_exp_ratio(evtfile, bgfile):
    """
    Calculate the ratio of exposure times of the science file vs. the
    background file.

    @param evtfile: event file path
    @type evtfile: string
    @param bgfile: background event file path
    @type bgfile: string

    @return: (float) ratio of exposure times
    """
    from .utils import require_file
    from .fits import gti_exp
    if not require_file([evtfile, bgfile]):
        print("Error: could not calculate exp ratio (file not found)")
        return False
    st = gti_exp(evtfile)  # evt3 exptime
    at = gti_exp(bgfile)  # acisbg exptime
    return 1. * st / at


def calc_backscal(evtfile, bgfile, ccd=None):
    """
    Calculate the background scaling value for an ACIS background dataset.
    This is applicable for a single observation. Also, front-illuminated chips
    (01234689) and back-illuminated chips (57) have different backgrounds and
    must be processed separately.

    The ratio of C{EXPOSURE} is compared with the ratio of 9.5-12.0 keV counts
    to derive a C{BACKSCAL} value such that::

        evtfile counts   evtfile EXPOSURE * evtfile BACKSCAL
        -------------- = -----------------------------------
        bgfile counts     bgfile EXPOSURE * bgfile BACKSCAL

    (This is how XSPEC interprets BACKSCAL values -- both the evtfile and
    bgfile can have it set to a value other than 1.0.)

    This function computes the value (evtfile BACKSCAL / bgfile BACKSCAL). The
    user must decide how this is to be split between evtfile and bgfile.

    Note that the result given by this function is the above rescaling ratio
    that is further reduced by a small amount, to account for the level of
    background that is present in the simulated readout background. This
    reduction is calculated from

    nominal exposure
    ----------------
    frame time

    where frame time = nominal exposure + readout time.

    @param evtfile: event file path
    @type evtfile: string
    @param bgfile: background event file path
    @type bgfile: string
    @param ccd: only calculate for specified ccd_id
    @type ccd: [int]

    @return: (float) C{BACKSCAL} value
    """
    from .utils import require_file
    from .eventfile import EventFile
    if not require_file([evtfile, bgfile]):
        print("Error: could not calculate bgbackscal (file not found)")
        return False
    evtfits = EventFile(evtfile)
    ctratio = calc_hardcount_ratio(evtfile, bgfile, ccd=ccd)
    expratio = calc_exp_ratio(evtfile, bgfile)
    frametime = evtfits.get_frametime()
    nominal = evtfits.get_nominal_exposure()
    reduction = nominal / frametime
    backscal = ctratio / expratio * reduction
    return backscal


def calc_bgimg_rescale(evtfile, bgfile, ccd=None):
    """
    Calculate ACIS background image scaling to match 9.5-12.0 keV counts with
    corresponding event file, then rescale it to account for the amount
    contained in the readout image. Use this for creating ACIS background
    images.

    @param evtfile: event file path
    @type evtfile: string
    @param bgfile: background event file path
    @type bgfile: string
    @param ccd: (optional) filter by C{ccd_id}
    @type ccd: [int]

    @return: (float)
    """
    from .utils import require_file
    from .eventfile import EventFile
    if not require_file([evtfile, bgfile]):
        print("Error: could not calculate bgimg rescale (file not found)")
        return False
    evtfits = EventFile(evtfile)
    frametime = evtfits.get_frametime()
    nominal = evtfits.get_nominal_exposure()
    ctratio = calc_hardcount_ratio(evtfile, bgfile, ccd=ccd)
    reduction = nominal / frametime
    rescale = ctratio * reduction
    return rescale


def update_fake_exposure(bgfile, exposure):
    """
    Update keywords TSTART, TSTOP, ONTIME, LIVETIME, and EXPOSURE in the
    background events file, and append a fake GTI extension, to effect the
    specified exposure time.

    Usage:

    After merging two blank sky background files, use this to update the
    header keywords and GTI extension.
    """
    from .eventfile import fake_gti
    import astropy.io.fits as pf

    assert (isinstance(exposure, int) or isinstance(
        exposure, float)), 'Exposure must be an int or float.'
    kw = {
        'TSTART': (0., 'Fake start time for background exposure'),
        'TSTOP': (float(exposure), 'Fake stop time for background exposure'),
        'ONTIME': float(exposure),
        'LIVETIME': float(exposure),
        'EXPOSURE': float(exposure)
    }

    bgf = pf.open(bgfile)
    for ext in bgf:
        for key in kw:
            if key in ext.header:
                ext.header[key] = kw[key]

    gtiext = fake_gti(exposure)

    bgf.append(gtiext)

    bgf.writeto(bgfile, overwrite=True, checksum=True)
