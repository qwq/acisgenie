
How to use
==========

> Most of these programs were created with interactive use in mind (the author
> uses it in IPython). In many cases, the user may check the docstring of a
> function for more detailed descriptions. In Python, this can be done by
> printing the `__doc__` attribute of an object. For example,
```python
import acisgenie
print(acisgenie.__doc__)
print(acisgenie.genie.lookup_obs_info.__doc__)
```

- Setup and requirements

This software is written in Python 3. It is intended to be used in an
environment that has been set up for the CIAO software. As of CIAO 4.13, only
Python 3 is supported and the default version is 3.8.

In order to preempt incompatibilities between packages installed by different
Python environments and CIAO's Python packages, it is recommended that the
user check for any Python packages they may have installed in their home
directory (e.g. via the `pip install --user`) and remove these, as well as
checking `sys.path` in Python to make sure no spurious directories are in the
package loading list.

CIAO 4.13 makes available the option to be installed as a conda environment,
through which Python packages may be managed. The alternative for the
non-conda CIAO installation is to set up an environment in conda with the
same Python version, and specifying the paths to `python` and `ipython` in
`.ciaorc`.

# Data reduction walkthrough

This is typically done in the following order:

- Reprocessing archival data

Obtain Level=1 files from the Chandra archive. Reprocess using a customized
mask for bad pixels. Generate readout background. Create region mask for point
sources. Create region mask for background region. Check for and filter
flares. Append a fake GTI extension to the readout background file.

- ACIS blank sky background

Identify appropriate blank sky background data set in archive. Apply VFAINT
filter if needed. Reproject events to science events. Apply the same bad
pixel filter.

- Extracting spectra

Set up a JSON configuration file. Create extraction region file. Extract
spectral products.

- Creating exposure corrected images

Update the JSON configuration file with ICM properties from spectral fit.
Choose energy bands. Create counts images and exposure maps.


## Reprocessing archival data

### Download files from archive

First, find the list of obsids to download. Using 4215 (Abell 520) as example,
get these related files:

```bash
download_chandra_obsid 4215 evt1,asol,bpix,bias,stat,msk,mtl,pbk
```

Go into the top directory containing the obsid directories, run this in a
Python session:

```python
import acisgenie
obs4215 = acisgenie.genie.lookup_obs_info(4215)
```

Next, create an `InfoObsGroup` object and add the `InfoObsFiles` object
returned by `lookup_obs_info` to it. Do this for every obsid.

```python
targetinfo = acisgenie.genie.InfoObsGroup()
targetinfo.update_obs(obs4215)
```

These classes `InfoFile`, `InfoEvtFile`, `InfoBgFile`, `InfoObsFiles`,
`InfoObsGroup` are defined in `acisgenie.genie`, and all of them have the
method `.get_object()` that returns their information content in a format
that can be stored as JSON.

Save and load information in `targetinfo` between sessions:

```python
# Save to JSON file
targetinfo.export_to_file('cluster_name.json', overwrite=True)

# Load from JSON file
targetinfo = acisgenie.genie.InfoObsGroup()
targetinfo.import_from_file('cluster_name.json')
```


### Reprocess Level=1 files to Level=2 files

> **Important**
>
> The procedure here departs from the CIAO thread
> (http://cxc.harvard.edu/ciao/threads/createL2/index.html) in bad pixel
> masking. Event files and exposure maps created by this procedure do not have
> the same bad pixel mask as the default products and should not be mixed
> carelessly.
>
> * `acis_build_badpix` is run with `usrfile=` set to a text file containing
>   the 9 rows of framestore shadow for each CCD.
> * `bitflag=00000000000000011111100010012111` is used instead of the default.
>   This setting only includes neighbors of bias=4095 (dead) pixels (flagged
>   with event status bit 6 by `acis_format_events`). Doing so includes these
>   pixels in the `bpix1.fits` file so that exposure maps created with CIAO
>   have the same mask when using `BPMASK=0x1F99F`.


While in the top level directory in the Python session, run

```python
for obskey in targetinfo._obs_obj:
   acisgenie.genie.script_process_evt2(targetinfo._obs_obj[obskey])
```

This creates a shell script `process_evt2` in every obsid directory, while
updating the information in `targetinfo` related to the obsids. Run this
script to do the reprocessing on Level=1 files to obtain Level=2 products.
For multiple obsid directories, a shell script such as the one for `bash`
below can be used.

```bash
for i in 4215 9424 9425 9426 9430
do
   cd $i; ./process_evt2 >& evt2.log; cd ..
done
```

Note that this reprocessing also creates a custom badpix file in `secondary/`,
which masks the pixels flagged by CIAO tools, as well as 9 rows along the CCD
edge affected by the framestore shadow (see code for details).

At this point, the archival `evt1.fits.gz` and intermediate product
`evt1_destreak.fits` may be removed, and the reprocessed `evt1.fits` file can
be gzipped to save space. The reprocessed `evt1.fits` be probably no longer
be needed after all the Level=2 files have been processed, and the ACIS
background and readout background files have been created.


### Select point sources

After reprocessing Level=1 files for all obsids, combine their 0.5-4 keV count
images. With the default directory structure and file naming pattern, this can
be done with `arith.exe` from ZHTOOLS.

```bash
# In directory above obsid folders
addimages */0540.img 0540_coadd.img
```

Load this image in DS9 and use it to select point sources. Greyscale color
setting, square root scale, and integer zoom level work well. Block the image
as you inspect sources further off-axis.

Save the regions it as a region file at `reg/pts.reg`. It should have the
format `ds9 wcs fk5`. Reinspect the image with the selected point sources cut
out in order to check for fainter sources that were less obvious, and update
`pts.reg` as needed.

```bash
dmcopy 0540_coadd.img'[exclude sky=region(reg/pts.reg)]' \!0540_coadd_noso.img
```

Similarly, select the regions that have any extended emission. These will be
used to mask regions for flare filtering. Save it as
`reg/lcclean_template.reg`.


### Flare-filtering Level=2 files

Now run the following to generate a file `process_evt3` as well as the
appropriate `lc_clean.par` and preliminary `lcclean.reg` files in
`primary/`. `targetinfo` is updated with more information about the obsids.

```python
for obskey in targetinfo._obs_obj:
   acisgenie.genie.script_process_evt3(targetinfo._obs_obj[obskey])
```

A helper script `view_lcclean_regs.sh` is placed in the obsid directories that
will launch `ds9` with the `lcclean.reg` region overlaid. Every observation
should be examined to ensure that the masking is done properly, and the
remaining region has enough area to yield a background light curve.

> The following paragraph can be skipped if there are no bright sources in the
> image that are creating significant readout artifacts.
>
> Before we proceed, open the image `4215/0540.img` in DS9, load the region
> file `4215/primary/lcclean.reg`, and check for readout artifacts along
> columns where there is any bright point sources (or even a bright cool
> core). If there are readout artifacts along the columns where bright sources
> are, these affected columns should be masked. To do this, select these
> columns using rotated rectangles and add these to `lcclean.reg`. If using
> DS9 to update `lcclean.reg`. The region file should be saved in `ds9 wcs
> fk5` format. Also ensure the FOV regions are in front (i.e. the positive
> regions must appear first in the text of the region file).

Running the following will call `lc_clean` with the preset configuration in
`lc_clean.par`, generate a clean GTI file, and plot the light curve. It will
then filter the science events, readout events, and aspect solution, and put
them in the `processed/` folder.

```bash
for i in 4215 9424 9425 9426 9430
do
   cd $i; ./process_evt3 >& evt3.log; cd ..
done
```

Check the content of `evt3.log` to see the output of `lc_clean` and whether
any errors occurred. View the light curve figure `primary/clean.lc.eps` to see
if manual input is needed.

By default, `lc_clean` filters based on the count rate. However,
`lc_clean.par` can be edited to perform a filter based on the ratio of soft to
intermediate band count rate instead, which is a measure of the stability of
the spectral shape of the ACIS background. See the instructions within.


### Append fake GTI to `readoutbg.fits`

The readout background needs a fake GTI extension to reduce its effective
count rate. Timed mode ACIS observations are typically either 3.1 s or 3.2 s
per frame, with 0.04104 s between frames when data is read out. During this
time photons continue to arrive. The effect of the fake GTI extension is to
scale the readout count rate proportionally, 0.04104/3.14014 or
0.04104/3.24014.

This can be done for every obsid in `targetinfo` with

```python
acisgenie.genie.fixReadoutbgGti(targetinfo)
```


## Processing ACIS blank sky background

Run the following to place a file `process_acisbg` in every obsid directory.
Information about the background files will also be updated in `targetinfo`.

```python
for obskey in targetinfo._obs_obj:
   acisgenie.genie.script_process_acisbg(targetinfo._obs_obj[obskey])
```

The shell scripts try to do the following: for every CCD it finds in the evt3
file, a CALDB query is performed to locate the relevant blank sky ACIS
background file. This is copied to the `processsed/` folder, and
`make_acisbg` is called to reproject the event coordinates to match the evt3
file. A bad pixel filter is then applied to match the evt3 file. The result
is a set of `acisbg?.fits` files, with `?` denoting the CCD ID from 0-9.

```bash
for i in 4215 9424 9425 9426 9430
do
   cd $i; ./process_acisbg >& acisbg.log; cd ..
done
```

Be sure to check every log file for errors.


## JSON file

For the last step, run this to update the rescaling factors for the readout
and ACIS backgrounds.

```python
acisgenie.genie.updateAcisbgImgRescale(targetinfo)
acisgenie.genie.updateReadoutbgImgRescale(targetinfo)
```

If there are partial entries in `targetinfo`, for example missing ACIS
background files, there will be printed messages to that effect.

Finally, call the `validate()` method of `targetinfo` to catch any obvious
issues.

```python
targetinfo.validate()
```

If this returns anything but `True`, there are issues that need to be
addressed. Otherwise, save this information.

```python
targetinfo.export_to_file('cluster_name.json', overwrite=True)
```

At this stage, there is a `processed/` folder for each obsid with the
flare-filtered events file `evt3.fits`, the good time intervals `clean.gti`,
GTI-filtered aspect file `asol1.fits`, background files `readoutbg.fits` and
`acisbg?.fits`. If there are back-illuminated chips in the observation, they
are in `evt3_BI.fits`, `clean_BI.fits`, `asol1_BI.fits`, and
`readoutbg_BI.fits`.
